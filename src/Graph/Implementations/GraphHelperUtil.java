/**
Copyright 2014 Dines Klewing Juul Madsen(thedrows@gmail.com), Heidi Olivia Munksgaard(heidi.olivi@gmail.com)

This file is part of GraphHelper.
GraphHelper is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
GraphHelper is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with GraphHelper. If not, see http://www.gnu.org/licenses/.
*/

package Graph.Implementations;

import Graph.InterestingPaths.InterestingPath;
import Graph.InterestingSearch.RDFTreeNode;
import com.hp.hpl.jena.query.Query;
import com.hp.hpl.jena.query.QueryFactory;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentLinkedQueue;

public class GraphHelperUtil {

    /**
     * Reduces literals to max 20 chars, and adds "..." to the end if longer
     *
     * @param _inputLiteral String to be cropped
     * @return Cropped string or original if shorter than 20
     */
    public static String cropLiteral(String _inputLiteral) {
        if (_inputLiteral.length() > 20) {
            return _inputLiteral.substring(0, 17) + "...";
        } else {
            return _inputLiteral;
        }
    }

    /**
     * Crops URI's by returning the substring following the last '#' or '/'
     *
     * @param _URI URI to be cropped
     * @return Cropped URI
     */
    public static String cropURI(String _URI) {
        String shortName = null;
        if (_URI.length() > 2) {
            int i;
            for (i = _URI.length() - 1; i >= 0; i--) {
                if (_URI.charAt(i) == '/' || _URI.charAt(i) == '#') {
                    break;
                }
            }
            shortName = _URI.substring(i + 1);
        }
        return shortName;
    }

    /**
     * Crops URI's by returning the substring following the last '#' or '/'. If
     * _prefix contains a matching prefix, the return value will be of the form
     * prefix:_URI
     *
     * @param _URI URI to be cropped
     * @param _prefix Prefixes to be searched
     * @return Cropped URI
     */
    public static String cropURI(String _URI, Map<String, String> _prefix) {
        String result = "";

        for (Map.Entry<String, String> z : _prefix.entrySet()) {
            if (_URI.startsWith(z.getValue())) {
                result = z.getKey() + ":" + GraphHelperUtil.cropURI(_URI);
            }
        }

        if ("".equals(result)) {
            result = GraphHelperUtil.cropURI(_URI);
        }
        return result;
    }

    /**
     * Determines whether string is valid URI using Java URI class
     *
     * @param _URI URI to be verified
     * @return true if correct URI, false otherwise
     */
    public static boolean isValidURI(String _URI) {
        try {
            URI uri = new URI(_URI);
            return true;
        } catch (URISyntaxException ex) {
            return false;
        }
    }

    /**
     * Transforms a literal string of the form "*" to either "*"@lang or "*"
     * with special characters escaped
     *
     * @param _literal String to be prepared
     * @return Escaped and language tagged string
     */
    public static String prepareLiteral(String _literal) {
        if (_literal.contains("@")) {
            for (int i = _literal.length() - 1; i >= 0; i--) {
                if (_literal.charAt(i) == '@') {

                }
            }
            return null;
        } else {
            return "\"" + _literal.replace("\"", "\\\"") + "\"";
        }
    }

    /**
     * Parses an array of Interesting paths and returns a distinct list URIs for
     * intermediate nodes
     *
     * @param _paths Array to be searched through
     * @return Array of distinct URIs
     */
    public static String[] DistinctURI(List<InterestingPath> _paths) {
        Set<String> result = new HashSet<>();

        for (InterestingPath p : _paths) {
            for (int i = 2; i < p.myPath.size(); i += 2) {
                if (!result.contains(p.myPath.get(i)) && isValidURI(p.myPath.get(i))) {
                    result.add(p.myPath.get(i));
                }
            }
        }

        String[] r = new String[result.size()];
        r = result.toArray(r);
        return r;
    }

    /**
     * Parses an array of Interesting paths and returns a distinct list URIs for
     * intermediate nodes
     *
     * @param _nodes set of nodes to be converted
     * @return Array of distinct URIs
     */
    public static String[] DistinctURI(Set<RDFTreeNode> _nodes) {
        String[] r = new String[_nodes.size()];
        int ptr = 0;
        for (RDFTreeNode tn : _nodes) {
            r[ptr] = tn.getNodeURI();
            ptr++;
        }
        return r;
    }

    /**
     * Parses an array of Interesting paths and returns a distinct list URIs for
     * intermediate nodes
     *
     * @param _paths Array to be searched through
     * @return Array of distinct URIs
     */
    public static String[] DistinctURI(ConcurrentLinkedQueue<InterestingPath> _paths) {
        Set<String> result = new HashSet<>();

        for (InterestingPath p : _paths) {
            for (int i = 2; i < p.myPath.size(); i += 2) {
                if (!result.contains(p.myPath.get(i)) && isValidURI(p.myPath.get(i))) {
                    result.add(p.myPath.get(i));
                }
            }
        }

        String[] r = new String[result.size()];
        r = result.toArray(r);
        return r;
    }

    /**
     * Determines whether string is valid integer
     *
     * @param _int Strign to be tested
     * @return Returns true iff _int is a valid integer
     */
    public static boolean isInteger(String _int) {
        boolean result = false;
        try {
            int i = Integer.parseInt(_int);
            result = true;
        } catch (NumberFormatException e) {
        }
        return result;
    }

    /**
     * Determines whether string is a valid natural number
     *
     * @param _int String to be tested
     * @return Returns true iff _int is integer > 0
     */
    public static boolean isNaturalNumber(String _int) {
        boolean result = false;
        try {
            int i = Integer.parseInt(_int);
            if (i > 0) {
                result = true;
            }
        } catch (NumberFormatException e) {
        }
        return result;
    }

    /**
     * Determines whether string is a natural number or zero
     *
     * @param _int String to be tested
     * @return Returns true iff _int is integer > -1
     */
    public static boolean isWholeNumber(String _int) {
        boolean result = false;
        try {
            int i = Integer.parseInt(_int);
            if (i > -1) {
                result = true;
            }
        } catch (NumberFormatException e) {
        }
        return result;
    }

    /**
     * Determines whether _double is a real number
     *
     * @param _double String to be tested
     * @return Returns true iff string is real number
     */
    public static boolean isRealNumber(String _double) {
        boolean result = false;
        try {
            Double.parseDouble(_double);
            result = true;
        } catch (NumberFormatException e) {
        }
        return result;
    }

    /**
     * Determines whether string is a real positive number
     *
     * @param _double String to be tested
     * @return Returns true iff _double is real positive number
     */
    public static boolean isRealPositiveNumber(String _double) {
        boolean result = false;
        try {
            double i = Double.parseDouble(_double);
            if (i > 0.0) {
                result = true;
            }
        } catch (NumberFormatException e) {
        }
        return result;
    }

    /**
     * Determines whether string is a real positive number
     *
     * @param _double String to be tested
     * @return Returns true iff _double is real positive number
     */
    public static boolean isRealNonNegativeNumber(String _double) {
        boolean result = false;
        try {
            double i = Double.parseDouble(_double);
            if (i >= 0.0) {
                result = true;
            }
        } catch (NumberFormatException e) {
        }
        return result;
    }

    /**
     * Validates an URI using Jena
     * @param _uri URI String to be validated
     * @return Returns true iff URI is valid for Jena
     */
    public static boolean isValidJenaURI(String _uri) {
        String query = String.format("DESCRIBE <%s>", _uri);
        try {
            Query q = QueryFactory.create(query);
            return true;
        } catch (Exception e) {
            return false;
        }
    }
    
    public static void main(String[] args) {
    }
}
