/**
Copyright 2014 Dines Klewing Juul Madsen(thedrows@gmail.com), Heidi Olivia Munksgaard(heidi.olivi@gmail.com)

This file is part of GraphHelper.
GraphHelper is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
GraphHelper is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with GraphHelper. If not, see http://www.gnu.org/licenses/.
*/

package Graph.Algorithm.PBFS;

import Graph.Implementations.GraphHelperUtil;
import Graph.Implementations.SPARQLFactory;
import com.hp.hpl.jena.query.QuerySolution;
import com.hp.hpl.jena.query.ResultSet;
import com.hp.hpl.jena.rdf.model.Property;
import com.hp.hpl.jena.rdf.model.RDFNode;
import com.hp.hpl.jena.rdf.model.Resource;
import com.hp.hpl.jena.rdf.model.ResourceFactory;
import com.hp.hpl.jena.rdf.model.Statement;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

public class DataWorkUnit1 extends WorkUnit1 {

    // <editor-fold defaultstate="collapsed" desc="Properties">
    private QueryDirection myMassiveDirection = QueryDirection.Ingoing;
    private boolean isMassive = false;
    private Set<String> DMBlackList;

    // Tracks result size and how deep into the set we are
    private int resultSize = 0, myOffset = 0;

    /**
     * Candidate URI's for the frontier
     */
    private final List<String> frontierCandidates = new LinkedList<>();
    /**
     * Set of Jena triple statements
     */
    private final List<Statement> statementsFound = new ArrayList<>();
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Constructor">
    public DataWorkUnit1(String[] _filters, List<String> _URIs, Set<String> _blacklist) {
        myURIs = _URIs;
        myFilters = _filters;
        DMBlackList = _blacklist;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Getters/Setters">
    public void setResultSize(int resultSize) {
        this.resultSize = resultSize;
    }

    public boolean isIsMassive() {
        return isMassive;
    }

    public int getResultSize() {
        return resultSize;
    }

    public void setIsMassive() {
        isMassive = true;
    }

    /**
     * Returns a List containing the frontier URI candidates
     *
     * @return List containing URIs
     */
    public List<String> getFrontierCandidates() {
        return frontierCandidates;
    }

    /**
     * Returns a set of Jena Statements found
     *
     * @return Set of Jena Statements
     */
    public List<Statement> getStatementsFound() {
        return statementsFound;
    }
    // </editor-fold>

    /**
     * Constructs a SPARQL query with the appropriate Offset. If offset is
     * bigger than result size then returns null, on which the extraction
     * process should be terminated.
     *
     * @return SPARQL query string or null
     */
    public String getQuery() {
        if (isMassive == false) {
            return NormalQuery();
        } else {
            return MassiveQuery();
        }
    }

    /**
     * Parses result set as either normal or massive work unit.
     *
     * @param _rs Result set to be parsed
     */
    public void parseResultSet(ResultSet _rs) {

        if (isMassive == false) {
            NormalParseResultSet(_rs);
        } else {
            MassiveParseResultset(_rs);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="Normal WorkUnit methods">
    private String NormalQuery() {
        if (myOffset <= resultSize) {
            String query = SPARQLFactory.GetNightbours(myURIs, myFilters, myOffset);
            myOffset += 10000;
            return query;
        } else {
            return null;
        }
    }

    private void NormalParseResultSet(ResultSet _rs) {
        QuerySolution qs;
        while (_rs.hasNext()) {
            qs = _rs.next();

            Iterator<String> itr = qs.varNames();

            while (itr.hasNext()) {
                switch (itr.next()) {
                    case "known":
                        break;
                    case "p1":
                        break;
                    case "p2":
                        break;
                    case "s":
                        Resource ingoing_sbj = qs.get("s").asResource();
                        Property ingoing_pred = ResourceFactory.createProperty(
                                qs.get("p2").asResource().getURI());
                        RDFNode ingoing_known = qs.get("known");

                        if (!frontierCandidates.contains(ingoing_sbj.getURI())
                                && GraphHelperUtil.isValidJenaURI(ingoing_sbj.getURI())
                                && !DMBlackList.contains(ingoing_sbj.getURI())) {
                            frontierCandidates.add(ingoing_sbj.getURI());
                        }

                        // Creating triple and inserting into list
                        Statement in_stmt = ResourceFactory.createStatement(
                                ingoing_sbj, ingoing_pred, ingoing_known);

                        if (!statementsFound.contains(in_stmt)) {
                            statementsFound.add(in_stmt);
                        }
                        break;
                    case "o":
                        Resource outgoing_known = qs.getResource("known").asResource();
                        Property outgoing_pred = ResourceFactory.createProperty(
                                qs.get("p1").asResource().getURI());
                        RDFNode outgoing_obj = qs.get("o");

                        // Adding to frontier candidates
                        if (!frontierCandidates.contains(outgoing_obj.asResource().getURI())
                                && GraphHelperUtil.isValidJenaURI(outgoing_obj.asResource().getURI())
                                && !DMBlackList.contains(outgoing_obj.asResource().getURI())) {
                            frontierCandidates.add(outgoing_obj.asResource().getURI());
                        }

                        // Adding to statements
                        Statement out_stmt = ResourceFactory.createStatement(
                                outgoing_known, outgoing_pred, outgoing_obj);

                        if (!statementsFound.contains(out_stmt)) {
                            statementsFound.add(out_stmt);
                        }
                        break;
                }
            }
        }
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Massive WorkUnit Methods">
    private String MassiveQuery() {
        switch (myMassiveDirection) {
            case Ingoing:
                return getInboundMassiveQuery();
            case Outgoing:
                return getOutboundMassiveQuery();
            case Finished:
                return null;
            default:
                return null;
        }
    }

    /**
     * Creates a massive inbound query
     *
     * @return SPARQL query string
     */
    private String getInboundMassiveQuery() {
        String query = SPARQLFactory.GetInQuery(this.myURIs.get(0), myFilters, myOffset);
        return query;
    }

    /**
     * Creates a massive outbound query
     *
     * @return SPARQL query string
     */
    private String getOutboundMassiveQuery() {
        String query = SPARQLFactory.GetOutQuery(myURIs.get(0), myFilters, myOffset);
        return query;
    }

    /**
     * Parses in- or outbound depending on QueryDirection flag
     *
     * @param _rs ResultSet to be parsed
     */
    private void MassiveParseResultset(ResultSet _rs) {
        if (myMassiveDirection == QueryDirection.Ingoing) {
            ParseMassiveResultsetIn(_rs);
        } else if (myMassiveDirection == QueryDirection.Outgoing) {
            ParseMassiveResultsetOut(_rs);
        }
    }

    /**
     * Parsing ResulSet of format ?s ?p ?known
     *
     * @param _rs ResultSet to be parsed
     */
    public void ParseMassiveResultsetIn(ResultSet _rs) {
        QuerySolution qs;
        Resource sbj;
        Property pred;
        RDFNode obj;
        Statement stmt;

        int counter = 0;

        while (_rs.hasNext()) {
            qs = _rs.next();
            sbj = qs.getResource("s");
            pred = ResourceFactory.createProperty(qs.getResource("p").getURI());
            obj = qs.get("known");

            // Adding URI to frontier
            if (!frontierCandidates.contains(sbj.getURI())
                    && GraphHelperUtil.isValidJenaURI(sbj.getURI())
                    && !DMBlackList.contains(sbj.getURI())) {
                frontierCandidates.add(sbj.getURI());
            }

            // Adding statement to list
            stmt = ResourceFactory.createStatement(sbj, pred, obj);
            if (!statementsFound.contains(stmt)) {
                statementsFound.add(stmt);
            }
            counter++;
        }

        // Deciding on next step
        if (counter < 10000) {
            myMassiveDirection = QueryDirection.Outgoing;
            myOffset = 0;
        } else if (myOffset >= 30000) {
            myMassiveDirection = QueryDirection.Outgoing;
            myOffset = 0;
        } else {
            myOffset += 10000;
        }
    }

    /**
     * Parsing resultset of format ?known ?p ?o
     *
     * @param _rs ResultSet to be parsed
     */
    public void ParseMassiveResultsetOut(ResultSet _rs) {
        QuerySolution qs;
        Resource sbj;
        Property pred;
        RDFNode obj;
        Statement stmt;

        int counter = 0;

        while (_rs.hasNext()) {
            qs = _rs.next();
            sbj = qs.getResource("known");
            pred = ResourceFactory.createProperty(qs.getResource("p").getURI());
            obj = qs.get("o");

            // Adding to frontier
            if (!frontierCandidates.contains(obj.asResource().getURI())
                    && GraphHelperUtil.isValidJenaURI(obj.asResource().getURI())
                    && !DMBlackList.contains(obj.asResource().getURI())) {
                frontierCandidates.add(obj.asResource().getURI());
            }

            // Adding to statements
            stmt = ResourceFactory.createStatement(sbj, pred, obj);
            if (!statementsFound.contains(stmt)) {
                statementsFound.add(stmt);
            }

            counter++;
        }

        // Deciding on next step
        if (counter < 10000) {
            myMassiveDirection = QueryDirection.Finished;
        } else if (myOffset >= 30000) {
            myMassiveDirection = QueryDirection.Finished;
        } else {
            myOffset += 10000;
        }
    }
    // </editor-fold>

}

enum QueryDirection {

    Outgoing,
    Ingoing,
    Finished
}
