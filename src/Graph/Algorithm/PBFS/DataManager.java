/**
Copyright 2014 Dines Klewing Juul Madsen(thedrows@gmail.com), Heidi Olivia Munksgaard(heidi.olivi@gmail.com)

This file is part of GraphHelper.
GraphHelper is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
GraphHelper is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with GraphHelper. If not, see http://www.gnu.org/licenses/.
*/

package Graph.Algorithm.PBFS;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Set;

public class DataManager {

    private int maxResultset = 10000, maxQueryLength = 8000, 
            maxMetaPackage = 2000;

    private final String[] myFilters;
    private Queue<String> myMetaQueue = new LinkedList<>();
    private Map<String, Integer> myResultSizes = new HashMap<>();

    private Queue<String> myDataQueue = new LinkedList<>();
    private Queue<String> myDataQueuePlusOne = new LinkedList<>();
    private Set<String> myBL = new HashSet<>();
    
    public InformationReporter myReport;
    
    /**
     * For new sub-graph construction
     * @param _seed Initial URI seed
     * @param _filters Filters to be used
     */
    public DataManager(String _seed, String[] _filters) {
        myMetaQueue.add(_seed);
        myDataQueue.add(_seed);
        myBL.add(_seed);
        myFilters = _filters;
    }
    public DataManager(String _seed, String[] _filters, InformationReporter _iReport) {
        myMetaQueue.add(_seed);
        myDataQueue.add(_seed);
        myBL.add(_seed);
        myFilters = _filters;
        myReport = _iReport;
    }
    
    /**
     * Restores a data manager in order to extend a graph
     * @param _frontier Frontier to start from
     * @param _filters Filters to be used
     */
    public DataManager(List<String> _frontier, String[] _filters) {
        myMetaQueue.addAll(_frontier);
        myDataQueue.addAll(_frontier);
        myBL.addAll(_frontier);
        myFilters = _filters;
    }

    /**
     * Queues and sets to null
     */
    public void Cleanup() {
        myMetaQueue = null;
        myDataQueue = null;
        myDataQueuePlusOne = null;
        myResultSizes = null;
        myBL = null;
    }

    /**
     * Returns a MetaWorkUnit, or null if the queue is empty
     * @return MetaWorkUnit or null
     */
    public synchronized MetaWorkUnit getMetaWorkUnit() {
        if (!myMetaQueue.isEmpty()) {
            MetaWorkUnit newUnit = new MetaWorkUnit(myFilters);
            List<String> list = new ArrayList<>();
            int count = maxMetaPackage;

            while (!myMetaQueue.isEmpty() && count > 0) {
                list.add(myMetaQueue.remove());
                count--;
            }
            newUnit.setURIs(list);
            myReport.current.metaUnitCount++;
            myReport.current.metaUnitSizes.add(list.size());
            return newUnit;
        } else {
            return null;
        }
    }

    /**
     * Returns the result of a MetaWorkUnit
     * @param _mwu Processed MetaWorkUnit
     */
    public synchronized void putMetaWorkUnit(MetaWorkUnit _mwu) {
        myResultSizes.putAll(_mwu.getMyResults());
    }

    /**
     * Returns a DataWorkUnit or null if queue is empty
     * @return DataWorkUnit to be processed
     */
    public synchronized DataWorkUnit getDataWorkUnit() {
        if (!myDataQueue.isEmpty()) {
            DataWorkUnit newUnit = new DataWorkUnit(myFilters);
            String base = myDataQueue.remove();

            int leftover = newUnit.CalculateSpace() + base.length();
            int resultSize;
            if (myResultSizes.containsKey(base)) {
                resultSize = myResultSizes.get(base);
            } else {
                // We have no record, treat as massive
                resultSize = 20000;
            }

            List<String> list = new ArrayList<>();
            list.add(base);

            while (!myDataQueue.isEmpty()
                    && (leftover + myDataQueue.peek().length()) < maxQueryLength
                    && myResultSizes.containsKey(myDataQueue.peek())
                    && (resultSize + this.myResultSizes.get(myDataQueue.peek())) < maxResultset) {
                leftover += myDataQueue.peek().length();
                resultSize += myResultSizes.get(myDataQueue.peek());
                list.add(myDataQueue.remove());
            }
            newUnit.setResultSize(resultSize);
            newUnit.setURIs(list);

            if (resultSize > 10000) {
                newUnit.setIsMassive();
            }
            myReport.current.dataUnitCount++;
            myReport.current.dataUnitSizes.add(list.size());
            return newUnit;
        } else {
            return null;
        }
    }

    /**
     * Returns DataWorkUnit, where the URIs will be added to the frontier
     * @param _dwu Processed DataWorkUnit returned
     */
    public synchronized void putDataWorkUnit(DataWorkUnit _dwu) {
        List<String> URIs = _dwu.getFrontierCandidates();

        for (String s : URIs) {
            if (!myBL.contains(s)) {
                myDataQueuePlusOne.add(s);
            }
        }
    }

    /**
     * Moves the frontier, and makes a new frontier+1. Generates new
     * black list as well.
     */
    public void MoveFrontier() {
        myBL = new HashSet<>();

        for (String s : myDataQueuePlusOne) {
            myBL.add(s);
        }

        // Exchanging frontiers and creating new frontier
        myDataQueue = myDataQueuePlusOne;
        myDataQueuePlusOne = new LinkedList<>();

        // Filling Meta Data queue
        myMetaQueue = new LinkedList<>(myDataQueue);
        long sum = 0;
        int count = 0;
        for(Map.Entry<String, Integer> key : myResultSizes.entrySet()) {
            if(key.getValue() <= 10000) {
                sum += key.getValue();
                count++;
            }
        }
        myReport.current.optimalNumOfPackages = ((double)sum)/((double)maxResultset);
        myReport.current.dataUnitCount = count;
        myResultSizes = new HashMap<>();
    }

    /**
     * Returns the current frontier of URI strings
     *
     * @return Returns list of URIs as strings
     */
    public List<String> getFrontier() {
        List<String> list = new ArrayList<>(myDataQueue);
        return list;
    }
}
