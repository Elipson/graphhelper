/**
Copyright 2014 Dines Klewing Juul Madsen(thedrows@gmail.com), Heidi Olivia Munksgaard(heidi.olivi@gmail.com)

This file is part of GraphHelper.
GraphHelper is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
GraphHelper is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with GraphHelper. If not, see http://www.gnu.org/licenses/.
*/

package Graph.Algorithm.PBFS;

import Graph.Implementations.StopWatch;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import org.apache.commons.collections4.map.HashedMap;

/**
 *
 * @author Elipson
 */
public class TimeMonitor {

    String myPID;
    StopWatch myWatch = new StopWatch();
    int numOfWorkUnits = 0;
    Map<String, String> mySeconds = new HashMap<>();
    List<Map<String, String>> myUnits = new ArrayList<>();

    public TimeMonitor(String _PID) {
        myPID = _PID;
    }

    public void StartTimer() {
        myWatch.start();
    }

    public void StopTimer(String _name) {
        myWatch.stop();
        Long val = myWatch.getElapsedTime();
        mySeconds.put(_name, val.toString());
    }

    public void NextUnit() {
        myUnits.add(mySeconds);
        mySeconds = new HashedMap<>();
        numOfWorkUnits++;
    }

    @Override
    public String toString() {
        String result = "PID: " + myPID;

        if (numOfWorkUnits > 0) {
            result += "\nWorkUnits processed: " + numOfWorkUnits;
        }
        for (Map<String, String> u : myUnits) {
            for (Map.Entry<String, String> e : u.entrySet()) {
                result += "\n" + e.getKey() + "\t\t" + e.getValue();
            }
        }
        result += "--------------------";
        return result;
    }
}
