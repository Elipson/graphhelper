/**
Copyright 2014 Dines Klewing Juul Madsen(thedrows@gmail.com), Heidi Olivia Munksgaard(heidi.olivi@gmail.com)

This file is part of GraphHelper.
GraphHelper is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
GraphHelper is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with GraphHelper. If not, see http://www.gnu.org/licenses/.
*/

package Graph.Algorithm.PBFS;

import java.util.Objects;

public class InformedNode implements Comparable<InformedNode> {

    String myURI;
    int myResultSize;

    public InformedNode(String _uri, int _resultSize) {
        myURI = _uri;
        myResultSize = _resultSize;
    }

    @Override
    public int compareTo(InformedNode o) {
        if (this.myResultSize == o.myResultSize) {
            return 0;
        } else if (this.myResultSize > o.myResultSize) {
            return -1;
        } else {
            return 1;
        }
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 73 * hash + Objects.hashCode(this.myURI);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final InformedNode other = (InformedNode) obj;
        if (!Objects.equals(this.myURI, other.myURI)) {
            return false;
        }
        return true;
    }
}
