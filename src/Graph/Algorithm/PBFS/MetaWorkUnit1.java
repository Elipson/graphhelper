/**
Copyright 2014 Dines Klewing Juul Madsen(thedrows@gmail.com), Heidi Olivia Munksgaard(heidi.olivi@gmail.com)

This file is part of GraphHelper.
GraphHelper is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
GraphHelper is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with GraphHelper. If not, see http://www.gnu.org/licenses/.
*/

package Graph.Algorithm.PBFS;

import Graph.Implementations.SPARQLFactory;
import com.hp.hpl.jena.query.QuerySolution;
import com.hp.hpl.jena.query.ResultSet;
import com.hp.hpl.jena.rdf.model.RDFNode;
import gnu.trove.map.hash.THashMap;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class MetaWorkUnit1 extends WorkUnit1 {

    Map<String, Integer> myResults;
    List<InformedNode> myNodes;
    
    public MetaWorkUnit1(String[] _filters, List<InformedNode> _nodes) {
        myFilters = _filters;
        myNodes = _nodes;
        myResults = new THashMap<>(myNodes.size());
        myURIs = new ArrayList<>(myNodes.size());
    }

    /**
     * Constructs a query string based on myURIs and myFilters
     *
     * @return SPARQL query string
     */
    public String getQuery() {
        for(InformedNode in : myNodes) {
            myURIs.add(in.myURI);
        }
        String query = SPARQLFactory.GetCount(myURIs, myFilters);
        return query;
    }

    /**
     * Parses a result set and adds the result to myResults
     *
     * @param _rs Result set to be parsed
     */
    public void parseResultSet(ResultSet _rs) {
        while (_rs.hasNext()) {
            QuerySolution qs = _rs.next();

            RDFNode n = qs.get("known");
            RDFNode nCount = qs.get("count");
            myResults.put(n.asResource().getURI(), nCount.asLiteral().getInt());
        }
        
        InformedNode in;
        for(int i = 0; i < myNodes.size(); i++) {
            in = myNodes.get(i);
            if(myResults.containsKey(in.myURI)) {
                in.myResultSize = myResults.get(in.myURI);
            }
        }
    }
}
