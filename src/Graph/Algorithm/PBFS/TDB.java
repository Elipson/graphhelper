/**
Copyright 2014 Dines Klewing Juul Madsen(thedrows@gmail.com), Heidi Olivia Munksgaard(heidi.olivi@gmail.com)

This file is part of GraphHelper.
GraphHelper is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
GraphHelper is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with GraphHelper. If not, see http://www.gnu.org/licenses/.
*/

package Graph.Algorithm.PBFS;

import com.hp.hpl.jena.query.Dataset;
import com.hp.hpl.jena.query.Query;
import com.hp.hpl.jena.query.QueryExecution;
import com.hp.hpl.jena.query.QueryExecutionFactory;
import com.hp.hpl.jena.query.QueryFactory;
import com.hp.hpl.jena.query.ReadWrite;
import com.hp.hpl.jena.query.ResultSet;
import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.Statement;
import com.hp.hpl.jena.tdb.TDBFactory;
import gnu.trove.set.hash.THashSet;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class TDB {

    // <editor-fold defaultstate="collapsed" desc="Properties">
    private String mySource;
    private int myLength;
    private String[] myFilters;
    private String myEndpoint;
    private Date myCreation;
    private File myPath = null;
    private Dataset myDS = null;
    private Model myModel = null;
    private boolean ready2Work = false;
    private List<Statement> myBufferedTriples;
    private int myBufferSize = 1000000;
    
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Constructors">
    public TDB(String _source,
            int _length,
            String[] _filters,
            String _endpoint) {

        mySource = _source;
        myLength = _length;
        myFilters = _filters;
        myEndpoint = _endpoint;
        myCreation = new Date();
    }

    /**
     * Crates a TDB from a directory provided
     *
     * @param _dir
     */
    public TDB(File _dir) {

        myPath = _dir;
        try {
            File xmlDoc = new File(_dir, "settings.xml");
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(xmlDoc);

            doc.getDocumentElement().normalize();
            NodeList nList = doc.getElementsByTagName("settings");
            Node node = nList.item(0);

            Element root = doc.getDocumentElement();
            mySource = root.getElementsByTagName("source").item(0).getTextContent();
            myEndpoint = root.getElementsByTagName("endpoint").item(0).getTextContent();
            myLength = Integer.parseInt(root.getElementsByTagName("length").item(0).getTextContent());

            List<String> filters = new ArrayList<>();
            nList = doc.getElementsByTagName("filters");

            for (int i = 0; i < nList.getLength(); i++) {
                Node nNode = nList.item(i);

                if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                    filters.add(((Element) nNode).getElementsByTagName("filter").item(0).getTextContent());
                }
            }
            myFilters = new String[filters.size()];
            filters.toArray(myFilters);

            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
            myCreation = sdf.parse(root.getElementsByTagName("creationdate").item(0).getTextContent());
        } catch (ParserConfigurationException | SAXException | IOException e) {
            System.out.println(e.toString());
        } catch (ParseException ex) {
            Logger.getLogger(TDB.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    // </editor-fold>

    /**
     * Opens a continuous connection to the model. Program must call CloseModel()
     * when done.
     * @return Model for TDB
     */
    public Model OpenModel() {
        if (ready2Work) {
            myDS.begin(ReadWrite.WRITE);
            return myModel;
        } else {
            return null;
        }
    }
    
    /**
     * Closes the continuous connection opened by OpenModel().
     */
    public void CloseModel() {
        if(ready2Work) {
            myDS.end();
        }
    }
    
    public boolean Connect() {
        if (myPath == null) {
            return false;
        } else {
            try {
                myDS = TDBFactory.createDataset(myPath.getAbsolutePath());
                myModel = myDS.getDefaultModel();
                ready2Work = true;
                return true;
            } catch (Exception e) {
                System.out.println(e.getMessage());
                return false;
            }
        }
    }

    /**
     * Attempts to close Model and Dataset for TDB
     */
    public void Close() {
        if (myModel != null) {
            myModel.close();
        }
        if (myDS != null) {
            myDS.close();
        }
    }

    public synchronized void InsertStatements(List<Statement> _stmts) {
        // Committing work to TDB
        if (ready2Work == true) {
            myDS.begin(ReadWrite.WRITE);
            try {
                myModel.add(_stmts);
                myDS.commit();
            } catch (Exception e) {
                System.out.println(e.getMessage());
            } finally {
                myDS.end();
            }
        }
    }

    /**
     * Buffers write of statements by only writing every 500.000 triple
     * A write can be forced by calling FlushWriteBuffer()
     * @param _stmts Statements to be buffered and potentially written
     */
    public synchronized void BufferedInsertStatements(List<Statement> _stmts) {
        if(myBufferedTriples == null) {
            myBufferedTriples = new ArrayList<>(this.myBufferSize);
        }
        
        myBufferedTriples.addAll(_stmts);
        if (myBufferedTriples.size() > myBufferSize) {
            InsertStatements(myBufferedTriples);
            myBufferedTriples.clear();
        }
    }

    /**
     * Writes the buffered triples by writing them to disk
     */
    public void FlushWriteBuffer() {
        if (myBufferedTriples != null && !myBufferedTriples.isEmpty()) {
            InsertStatements(myBufferedTriples);
            myBufferedTriples.clear();
        }
    }
    /**
     * Compares this TDB with the source and endpoint URI. In addition the
     * filters provided must contain a stub-set of at least the same filters of
     * the object, but can contain more.
     *
     * @param _source
     * @param _endpoint
     * @param _filters
     * @return True iff source and endpoint match, and myFilters is a subset of
     * _filters.
     */
    public boolean Compare(String _source, String _endpoint, String[] _filters) {
        if (!Objects.equals(this.mySource, _source)) {
            return false;
        }
        if (!Objects.equals(this.myEndpoint, _endpoint)) {
            return false;
        }
        for (String s : myFilters) {
            if (!Arrays.asList(_filters).contains(s)) {
                return false;
            }
        }
        return true;
    }

    /**
     * Writes object settings to myPath/settings.xml
     *
     * @return
     */
    public boolean SaveSettings() {
        try {
            DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = docFactory.newDocumentBuilder();

            // root elements
            Document doc = docBuilder.newDocument();
            Element rootElement = doc.createElement("settings");
            doc.appendChild(rootElement);

            // Source URI
            Element nSource = doc.createElement("source");
            nSource.appendChild(doc.createTextNode(mySource));
            rootElement.appendChild(nSource);

            // Endpoint
            Element nEndpoint = doc.createElement("endpoint");
            nEndpoint.appendChild(doc.createTextNode(myEndpoint));
            rootElement.appendChild(nEndpoint);

            // Length
            Element nLength = doc.createElement("length");
            nLength.appendChild(doc.createTextNode(((Integer) myLength).toString()));
            rootElement.appendChild(nLength);

            // Creation date
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
            String date = sdf.format(myCreation);
            Element creationdate = doc.createElement("creationdate");
            creationdate.appendChild(doc.createTextNode(date));
            rootElement.appendChild(creationdate);

            // Filters
            Element filters = doc.createElement("filters");

            for (String f : myFilters) {
                Element filter = doc.createElement("filter");
                filter.appendChild(doc.createTextNode(f));
                filters.appendChild(filter);
            }

            rootElement.appendChild(filters);

            TransformerFactory tf = TransformerFactory.newInstance();
            Transformer t = tf.newTransformer();
            doc.setXmlVersion("1.0");
            t.setOutputProperty(OutputKeys.METHOD, "xml");
            t.setOutputProperty(OutputKeys.INDENT, "yes");
            t.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");
            t.setOutputProperty(OutputKeys.VERSION, "1.0");
            t.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
            File settingsFile = new File(myPath, "settings.xml");
            DOMSource source = new DOMSource(doc);
            StreamResult result = new StreamResult(settingsFile);

            t.transform(source, result);

            return true;
        } catch (ParserConfigurationException | DOMException | TransformerConfigurationException e) {
            return false;
        } catch (TransformerException ex) {
            Logger.getLogger(TDB.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    /**
     * Hash code is generated based on source and endpoint URI.
     *
     * @return
     */
    @Override
    public int hashCode() {
        int hash = 5;
        hash = 89 * hash + Objects.hashCode(this.mySource);
        hash = 89 * hash + Objects.hashCode(this.myEndpoint);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final TDB other = (TDB) obj;
        if (!Objects.equals(this.mySource, other.mySource)) {
            return false;
        }
        if (!Objects.equals(this.myEndpoint, other.myEndpoint)) {
            return false;
        }
        for (String s : myFilters) {
            if (!Arrays.asList(other.myFilters).contains(s)) {
                return false;
            }
        }
        return true;
    }

    // <editor-fold defaultstate="collapsed" desc="Read/Write frontier">
    /**
     * reads the frontier.txt file from myPath/frontier.txt
     *
     * @return Returns a list if successful or null if an error was encountered
     */
    public List<String> ReadFrontier() {
        List<String> list = new ArrayList<>();
        String line;
        try {
            FileReader fr = new FileReader(new File(myPath, "frontier.txt"));
            BufferedReader br = new BufferedReader(fr);
            line = br.readLine();

            while (line != null) {
                list.add(line);
                line = br.readLine();
            }
            br.close();
            fr.close();
            return list;
        } catch (FileNotFoundException ex) {
            return null;
        } catch (IOException ex) {
            return null;
        }
    }

    /**
     * Writes provided frontier to myPath/frontier.txt
     *
     * @param _frontier Frontier to be written
     * @return True iff entire frontier was written to file
     */
    public boolean WriteFrontier(List<String> _frontier) {
        try {
            try (PrintWriter writer = new PrintWriter(new File(myPath, "frontier.txt"))) {
                for (String s : _frontier) {
                    writer.println(s);
                }
                return true;
            }
        } catch (FileNotFoundException ex) {
            return false;
        }
    }

        public boolean WriteFrontier(Set<String> _frontier) {
        try {
            try (PrintWriter writer = new PrintWriter(new File(myPath, "frontier.txt"))) {
                for (String s : _frontier) {
                    writer.println(s);
                }
                return true;
            }
        } catch (FileNotFoundException ex) {
            return false;
        }
    }
    /**
     * Writes provided frontier to myPath/frontier.txt
     *
     * @param _frontier Frontier to be written
     * @return True iff entire frontier was written to file
     */
    public boolean WriteFrontier(String[] _frontier) {
        try {
            try (PrintWriter writer = new PrintWriter(new File(myPath, "frontier.txt"))) {
                for (String s : _frontier) {
                    writer.println(s);
                }
                return true;
            }
        } catch (FileNotFoundException ex) {
            return false;
        }
    }

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Getters/Setters">
    public String getMySource() {
        return mySource;
    }

    public int getMyLength() {
        return myLength;
    }

    /**
     * Increments the length and saves settings
     */
    public void IncrementLength() {
        myLength++;
        this.SaveSettings();
    }

    public String[] getMyFilters() {
        return myFilters;
    }

    public String getMyEndpoint() {
        return myEndpoint;
    }

    public Date getMyCreation() {
        return myCreation;
    }

    public File getMyPath() {
        return myPath;
    }

    public void setMyPath(File myPath) {
        this.myPath = myPath;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="SPARQL Methods">
    /**
     * Performs a construct SPARL on the TDB model
     *
     * @param _query Construct query to be executed
     * @return Model constructed or null if error
     */
    public Model performConstruct(String _query) {
        Model m = null;
        if (ready2Work) {
            myDS.begin(ReadWrite.READ);
            try {
                Query q = QueryFactory.create(_query);
                QueryExecution qExe = QueryExecutionFactory.create(q, myModel);
                m = qExe.execConstruct();
                qExe.close();
            } catch (Exception e) {
                System.out.println(e.getMessage());
            } finally {
                myDS.close();
            }
        }
        return m;
    }

    /**
     * Performs a select SPARQL query on TDB model
     *
     * @param _query Query to be performed
     * @return ResultSet of query
     */
    public ResultSet performSelect(String _query) {
        ResultSet rs = null;
        if (ready2Work == true) {
            myDS.begin(ReadWrite.READ);
            try {
                Query q = QueryFactory.create(_query);
                QueryExecution qExe = QueryExecutionFactory.create(q, myModel);
                rs = qExe.execSelect();
            } catch (Exception e) {
                System.out.println(e.getMessage());
            } finally {
                myDS.end();
            }
        }
        return rs;
    }

    public boolean performAsk(String _query) {
        boolean result = false;
        if (ready2Work == true) {
            myDS.begin(ReadWrite.READ);
            try {
                Query myQuery = QueryFactory.create(_query);
                QueryExecution qExe = QueryExecutionFactory.create(myQuery, myModel);
                result = qExe.execAsk();
                qExe.close();
            } catch (Exception e) {
                System.out.println(e.getMessage());
            } finally {
                myDS.end();
            }
        }
        return result;
    }
    // </editor-fold>
}
