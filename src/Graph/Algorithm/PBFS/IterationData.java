/**
Copyright 2014 Dines Klewing Juul Madsen(thedrows@gmail.com), Heidi Olivia Munksgaard(heidi.olivi@gmail.com)

This file is part of GraphHelper.
GraphHelper is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
GraphHelper is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with GraphHelper. If not, see http://www.gnu.org/licenses/.
*/

package Graph.Algorithm.PBFS;

import java.util.ArrayList;
import java.util.List;

public class IterationData {

    public IterationData(int _itr) {
        Iteration = _itr;
    }
    
    public int Iteration;
    public int metaUnitCount = 0;
    public List<Integer> metaUnitSizes = new ArrayList<>();
    public int dataUnitCount = 0;
    public List<Integer> dataUnitSizes = new ArrayList<>();
    public long metaPhase = 0;
    public long dataPhase = 0;
    public long movePhase = 0;
    public double optimalNumOfPackages = 0;
    
    @Override
    public String toString() {
        String result = "";

        result += "Iteration: " + Iteration;
        result += "\nMetaPackages: " + metaUnitCount;
        result += "\nOptimal Packaging: " + optimalNumOfPackages;
        result += "\nDataPackages: " + dataUnitCount;
        result += "\nMeta Phase(ms): " + metaPhase;
        result += "\nData Phase(ms): " + dataPhase;
        result += "\nMove Phase(ms): " + movePhase;
        result += "\n------------ MetaUnit sizes ------------";
        for (Integer i : metaUnitSizes) {
            result += "\n" + i;
        }
        result += "\n------------ DataUnit sizes ------------";
        for (Integer i : dataUnitSizes) {
            result += "\n" + i;
        }
        return result;
    }
}
