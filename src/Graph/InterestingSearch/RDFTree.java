/**
Copyright 2014 Dines Klewing Juul Madsen(thedrows@gmail.com), Heidi Olivia Munksgaard(heidi.olivi@gmail.com)

This file is part of GraphHelper.
GraphHelper is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
GraphHelper is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with GraphHelper. If not, see http://www.gnu.org/licenses/.
*/

package Graph.InterestingSearch;

import com.hp.hpl.jena.rdf.model.Model;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

public class RDFTree {

    private final Set<RDFTreeNode> myNodes = new HashSet<>();
    private RDFTreeNode myRoot = null;
    private int myDepth = 0;

    public RDFTree(RDFTreeNode _root) {
        myNodes.add(_root);
        myRoot = _root;
        myRoot.setMyDistance(0);
        myDepth = 0;
    }

    /**
     *
     * @return Returns number of nodes in tree
     */
    public int getCount() {
        return myNodes.size();
    }

    /**
     * @return Returns depth of tree
     */
    public int getDepth() {
        return myDepth;
    }

    /**
     * Checks whether a node is in the tree
     *
     * @param _node Node to be found
     * @return Returns true iff node is found in tree
     */
    public boolean contains(RDFTreeNode _node) {
        return this.myNodes.contains(_node);
    }

    /**
     * Adds a node to the parent provided.
     *
     * @param _parent Node to which the child is to be added
     * @param _newNode Node to be added
     * @return Returns true iff _parent was found in tree
     */
    public boolean addNode(RDFTreeNode _parent, RDFTreeNode _newNode) {
        if (myNodes.contains(_parent)) {
            _parent.addChild(_newNode);
            myNodes.add(_newNode);
            _newNode.setMyDistance(_parent.getMyDistance() + 1);
            if (_newNode.getMyDistance() > myDepth) {
                myDepth = _newNode.getMyDistance();
            }
            return true;
        } else {
            return false;
        }
    }

    /**
     * Removes a node from the tree.
     *
     * @param _node Node to be removed
     * @return Returns true iff _node is found in tree
     */
    public boolean removeNode(RDFTreeNode _node) {
        if (myNodes.contains(_node)) {
            _node.getMyParent().removeChild(_node);
            _node.setMyParent(null);
            myNodes.remove(_node);
            return true;
        } else {
            return false;
        }
    }

    /**
     * Returns the current root node.
     *
     * @return Root node for tree.
     */
    public RDFTreeNode getRoot() {
        return myRoot;
    }

    /**
     * Performs a deep copy of the tree nodes and returns it. The new tree has
     * independent nodes and hashmap.
     *
     * @return Deep copy of tree
     */
    public RDFTree copy() {
        // Copying nodes
        RDFTreeNode newRoot = myRoot.copy();
        // Inserting into new tree
        RDFTree result = new RDFTree(myRoot.copy());
        result.regenerateHashMap();
        return result;
    }

    /**
     * Ensures all nodes are registered correctly with the tree
     */
    public void regenerateHashMap() {
        myRoot.registerWithTree(myNodes);
    }
    
    public void printNodes(PrintWriter _writer) {
        myRoot.write(_writer);
    }
    
    /**
     * Returns a Set of the trees nodes. The Set should not be modified in
     * any way.
     * @return 
     */
    public Set<RDFTreeNode> getNodes() {
        return myNodes;
    }
    
    /**
     * Returns an array of node URIs for the tree
     * @return Array of URI strings
     */
    public String[] getNodeURIs() {
        String[] result = new String[myNodes.size()];
        int ptr = 0;
        for(RDFTreeNode tn : myNodes) {
            result[ptr] = tn.getNodeURI();
        }
        return result;
    }
    /**
     * Performs a depth first walk of the tree, generating 
     * an InterestingTriple for each node<>child relation
     * @param _model Model from which the tree was generated
     * @return Tree turned into ArrayList of Interesting Triples
     */
    public ArrayList<InterestingTriple> getInterestingTriples(Model _model)
    {
        ArrayList<InterestingTriple> results = new ArrayList<>();
        myRoot.getLocalTriples(results, _model);
        return results;
    }
}
