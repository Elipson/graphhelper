/**
Copyright 2014 Dines Klewing Juul Madsen(thedrows@gmail.com), Heidi Olivia Munksgaard(heidi.olivi@gmail.com)

This file is part of GraphHelper.
GraphHelper is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
GraphHelper is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with GraphHelper. If not, see http://www.gnu.org/licenses/.
*/

package Graph.InterestingSearch;

import Graph.Enumerator.NodeType;
import Graph.Enumerator.PredicateDirection;
import Graph.Implementations.GraphHelperUtil;
import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.RDFNode;
import com.hp.hpl.jena.rdf.model.Statement;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.Set;

public class RDFTreeNode implements Comparable<RDFTreeNode> {

    private RDFNode myValue;
    private RDFTreeNode myParent;
    private List<RDFTreeNode> myChildren = new LinkedList<>();
    private double myMatchScore = 0;
    private int myDistance = 0;

    // <editor-fold defaultstate="collapsed" desc="Constructors">
    public RDFTreeNode(RDFNode _value) {
        this.myValue = _value;
    }

    public RDFTreeNode(RDFNode _value, RDFTreeNode _parent) {
        this.myValue = _value;
        this.myParent = _parent;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Setters">
    public void setMyValue(RDFNode myValue) {
        this.myValue = myValue;
    }

    public void setMyParent(RDFTreeNode myParent) {
        this.myParent = myParent;
    }

    public void setMyChildren(List<RDFTreeNode> myChildren) {
        this.myChildren = myChildren;
    }

    public void setMyMatchScore(double myMatchScore) {
        this.myMatchScore = myMatchScore;
    }

    public void setMyDistance(int myDistance) {
        this.myDistance = myDistance;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Getters">
    public RDFNode getMyValue() {
        return myValue;
    }

    public RDFTreeNode getMyParent() {
        return myParent;
    }

    public List<RDFTreeNode> getMyChildren() {
        return myChildren;
    }

    public double getMyMatchScore() {
        return myMatchScore;
    }

    public int getMyDistance() {
        return myDistance;
    }
    // </editor-fold>

    /**
     * Adds a child to the node. Also sets child's parent to 'this'.
     *
     * @param _newChild Child to be added
     */
    public void addChild(RDFTreeNode _newChild) {
        if (!this.myChildren.contains(_newChild)) {
            _newChild.setMyParent(this);
            this.myChildren.add(_newChild);
        }
    }

    /**
     * Removes the provided child if it is in the list.
     *
     * @param _child Child to be removed
     */
    public void removeChild(RDFTreeNode _child) {
        if (myChildren.contains(_child)) {
            myChildren.remove(_child);
        }
    }

    /**
     * Returns the sum of the nodes distance from the root and it's match score
     *
     * @return (Distance - Score)
     */
    public double getScore() {
        return (((double)myDistance) - myMatchScore);
    }

    /**
     * Calculates a hash value for the node. As myValue will either be a
     * literal, blank node or URI, the value will always be unique.
     *
     * @return Returns hash value
     */
    @Override
    public int hashCode() {
        return this.myValue.hashCode();
    }

    /**
     * Compares two objects
     *
     * @param obj Object to be compared
     * @return Returns true iff objects are both RDFTreeNode and myValue for
     * both objects are identical. myValue can be used for comparison as it will
     * either be a Bnode, URI or literal, which has to be unique.
     */
    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final RDFTreeNode other = (RDFTreeNode) obj;
        return Objects.equals(this.myValue, other.myValue);
    }

    /**
     * Performs a deep copy of a node and all it's children. Method is called on
     * root by RDFTree.
     *
     * @return A deep copy of the node and it's children
     */
    public RDFTreeNode copy() {
        RDFTreeNode newNode = new RDFTreeNode(this.myValue);
        newNode.setMyDistance(this.myDistance);
        newNode.setMyMatchScore(this.myMatchScore);

        if (myChildren != null && !myChildren.isEmpty()) {
            for (RDFTreeNode tn : myChildren) {
                RDFTreeNode temp = tn.copy();
                temp.setMyParent(newNode);
                newNode.addChild(temp);
            }
        }
        return newNode;
    }

    /**
     * Recursively registers a node and it's children to the hash map provided.
     * Should be called on root node after a tree copy.
     *
     * @param _treeNodes Hash map to register nodes with
     */
    public void registerWithTree(Set<RDFTreeNode> _treeNodes) {
        // Registering node...
        if (!_treeNodes.contains(this)) {
            _treeNodes.add(this);
        }

        // Registering children
        for (RDFTreeNode tn : myChildren) {
            tn.registerWithTree(_treeNodes);
        }
    }

    @Override
    public int compareTo(RDFTreeNode compareNode) {
        if (getMyMatchScore() == compareNode.getMyMatchScore()) {
            return 0;
        } else if (getMyMatchScore() > compareNode.getMyMatchScore()) {
            return -1;
        } else {
            return 1;
        }
    }

    /**
     * Creates an entry in _cont for each child
     *
     * @param _cont List to write triples to
     * @param _model Model from which node was generated
     */
    public void getLocalTriples(ArrayList<InterestingTriple> _cont, Model _model) {
        if (myChildren != null && !myChildren.isEmpty()) {
            for (RDFTreeNode cld : myChildren) {
                InterestingTriple tlp = new InterestingTriple();

                // Node one values
                tlp.nodeOne = this.getMyValue().toString();
                tlp.nodeValueOne = (float) getMyMatchScore();
                if (getMyValue().isAnon()) {
                    tlp.nodeTypeOne = NodeType.BlankNode;
                    tlp.nodeOneShort = "BN";
                } else {
                    tlp.nodeTypeOne = NodeType.Vertex;
                    tlp.nodeOneShort = GraphHelperUtil.cropURI(tlp.nodeOne, _model.getNsPrefixMap());
                }

                // Node two values
                tlp.nodeTwo = cld.getMyValue().toString();
                tlp.nodeValueTwo = (float) cld.getMyMatchScore();
                if (cld.getMyValue().isAnon()) {
                    tlp.nodeTypeTwo = NodeType.BlankNode;
                    tlp.nodeTwoShort = "BN";
                } else {
                    tlp.nodeTypeTwo = NodeType.Vertex;
                    tlp.nodeTwoShort = GraphHelperUtil.cropURI(tlp.nodeTwo, _model.getNsPrefixMap());
                }

                // Exploring predicates
                List<Statement> sbj2obj = _model.listStatements(getMyValue().asResource(), null, cld.getMyValue()).toList();
                List<Statement> obj2sbj = _model.listStatements(cld.getMyValue().asResource(), null, getMyValue()).toList();

                // We have predicates going both ways
                if (!sbj2obj.isEmpty() && !obj2sbj.isEmpty()) {
                    tlp.predDir = PredicateDirection.uniDirection;
                } else if (sbj2obj.isEmpty() && !obj2sbj.isEmpty()) {
                    tlp.predDir = PredicateDirection.target2Source;
                } else {
                    tlp.predDir = PredicateDirection.source2Target;
                }

                sbj2obj.addAll(obj2sbj);
                tlp.predicateValues = new String[sbj2obj.size()];
                tlp.predicateShorts = new String[sbj2obj.size()];

                for (int i = 0; i < sbj2obj.size(); i++) {
                    tlp.predicateValues[i] = sbj2obj.get(i).getPredicate().toString();
                    tlp.predicateShorts[i] = GraphHelperUtil.cropURI(sbj2obj.get(i).getPredicate().toString(), _model.getNsPrefixMap());
                }

                _cont.add(tlp);

                // iterating over children
                cld.getLocalTriples(_cont, _model);
            }
        }
    }

    public void write(PrintWriter _writer) {
        _writer.println(
                myValue + "\t"
                + myMatchScore + "\t"
                + myDistance + "\t"
                + myChildren.size());

        if (myChildren != null && myChildren.size() > 0) {
            for (RDFTreeNode cld : myChildren) {
                cld.write(_writer);
            }
        }
    }
    
    public String getNodeURI() {
        return this.myValue.asResource().getURI();
    }
    
    public boolean hasChildren() {
        if(myChildren == null) {
            return false;
        } else if (myChildren.isEmpty()) {
            return false;
        } else {
            return true;
        }
    }
}
