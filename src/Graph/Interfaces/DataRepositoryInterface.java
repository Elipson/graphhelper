/**
Copyright 2014 Dines Klewing Juul Madsen(thedrows@gmail.com), Heidi Olivia Munksgaard(heidi.olivi@gmail.com)

This file is part of GraphHelper.
GraphHelper is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
GraphHelper is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with GraphHelper. If not, see http://www.gnu.org/licenses/.
*/

package Graph.Interfaces;

import GUI.Threads.InterestingGraphThread;
import GUI.Threads.InterestingPathsThread;
import Graph.Enumerator.DeleteEdge;
import Graph.Enumerator.DeleteModel;
import Graph.Enumerator.ExportModel;
import Graph.Enumerator.FileFormat;
import Graph.Enumerator.DeleteLiteral;
import Graph.Enumerator.InsertLiteral;
import Graph.Enumerator.UpdateLiteralText;
import Graph.Enumerator.ImportModel;
import Graph.Enumerator.NodeType;
import Graph.Enumerator.RunAskResult;
import Graph.Enumerator.SPARQLExpressionType;
import Graph.Enumerator.DeleteVertex;
import Graph.InterestingSearch.InterestingLiterals;
import Graph.Implementations.ResourceResult;
import Graph.Implementations.SPARQL_ResultSet;
import Graph.Implementations.TripleStatement;
import Graph.Implementations.TripleStatementWrapper;
import Graph.Implementations.VisualiseWrapper;
import Graph.InterestingPaths.DisambiguityResults;
import Graph.InterestingPaths.InterestingPath;
import java.util.ArrayList;

public interface DataRepositoryInterface {

    // Models commands
    public DeleteModel DeleteModel(String _model);
    public ExportModel ExportModel(String _model, String _path, FileFormat _format);
    public ImportModel ImportModel(String _path, String _modelName, FileFormat _format);
    public void ImportGenericHTTPEndPoint(String _URL, String _name);
    public String[] GetModelNames();
    public boolean ContainsModel(String _model);
    public TripleStatementWrapper InterestingSearch(
            String _model, String _bagOfWords, 
            int _maxNodes, int _maxDist, 
            String _initNode, NodeType _initType, 
            double _beta, String[] _filters, InterestingGraphThread _report,
            String _lang);
    
    public void setMaxVertices(String _model, int _max);
    public int getMaxVertices(String _model);
    public VisualiseWrapper CenterOn(String _model, String _center, NodeType _type);
    public VisualiseWrapper CenterOn(String _model);
    public DisambiguityResults DisambiguitySearch(String _model, String _searchTerm, String _lang);
    
    public InterestingPath[] InterestingPaths(String _model, String[] _NSFilters,
            String _source, String _target, String _searchTerms,
            String _lang, int _minPath, int _maxPath, int _maxNumOfPaths, InterestingPathsThread _report);
    
    // Individual model commands
    public boolean RenameModel(String _model, String _newName);
    public ArrayList<TripleStatement> GetStatements(String _model);
    public boolean isLocalGraph(String _model);
    public boolean TestConnection(String _model);
    public String getCenterVertex(String _model);
    public void setCentervertex(String _model, String _newVertex, NodeType _node);
    public NodeType getNodeType(String _model, String _value);
    public int getTimeout(String _model);
    public void setTimeout(String _model, int _timeout);
    public String getEndpointURI(String _model);
    public void setEndpointURI(String _model, String _newURI);
    
    // Vertex commands
    public ResourceResult UpdateVertexURI(String _model, String _vertex, String _newURI);
    public ResourceResult VertexToBlankNode(String _model, String _vertex);
    public DeleteVertex DeleteVertex(String _model, String _node);
    public ResourceResult InsertVertex(String _model, String _value);
    public ArrayList<TripleStatement> CBD(String _model, String _vertex);
    
    // Blank Node commands
    public ResourceResult ResolveBlankNode(String _model, String _nodeID, String _newURI);
    public DeleteVertex DeleteBlankNode(String _model, String _nodeID);
    public ResourceResult InsertBlankNode(String _model);
    
    // Literal commands
    public UpdateLiteralText UpdateLiteralText(String _model, String _literal, String _newText);
    public DeleteLiteral DeleteLiteral(String _model, String _literal);
    public InsertLiteral InsertLiteral(String _model, String _text);
    public ArrayList<TripleStatement> SCBD(String _model, String _literal);
    public InterestingLiterals getLiterals(String _model, String _center, NodeType _centerType);
    
    // Edge commands
    public DeleteEdge DeleteEdge(String _model, 
            String _edgeURI, String _source, 
            NodeType _sourceType, String _target, 
            NodeType _targetType);
    public ResourceResult ChangeEdgeURI(String _model, String _edgeURI, String _source, NodeType _sourceType, String _target, NodeType _targetType, String _newEdgeURI);
    public ResourceResult InsertEdge(String _model, String _edgeURI, String _source, NodeType _sourceType, String _target, NodeType _targetType);
    
    // SPARQL commands
    public SPARQLExpressionType getQueryType(String _query);
    public SPARQL_ResultSet RunSelect(String _query, String _model);
    public void RunConstruct(String _query, String _model);
    public RunAskResult RunAsk(String _model, String _query);
}
