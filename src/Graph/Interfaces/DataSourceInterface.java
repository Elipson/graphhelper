/**
Copyright 2014 Dines Klewing Juul Madsen(thedrows@gmail.com), Heidi Olivia Munksgaard(heidi.olivi@gmail.com)

This file is part of GraphHelper.
GraphHelper is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
GraphHelper is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with GraphHelper. If not, see http://www.gnu.org/licenses/.
*/

package Graph.Interfaces;

import GUI.Threads.InterestingGraphThread;
import GUI.Threads.InterestingPathsThread;
import Graph.Enumerator.DeleteEdge;
import Graph.Enumerator.DeleteLiteral;
import Graph.Enumerator.InsertLiteral;
import Graph.Enumerator.UpdateLiteralText;
import Graph.Enumerator.NodeType;
import Graph.Enumerator.ReadyToVisualise;
import Graph.Enumerator.DeleteVertex;
import Graph.InterestingSearch.InterestingLiterals;
import Graph.Implementations.ResourceResult;
import Graph.Implementations.SPARQL_ResultSet;
import Graph.Implementations.TripleStatement;
import Graph.Implementations.TripleStatementWrapper;
import Graph.Implementations.VisualiseWrapper;
import Graph.InterestingPaths.DisambiguityResults;
import Graph.InterestingPaths.InterestingPath;
import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.sparql.engine.http.QueryExceptionHTTP;
import java.util.ArrayList;

public interface DataSourceInterface {
    public SPARQL_ResultSet performSelect(String _query);
    public Model performConstruct(String _query) throws QueryExceptionHTTP;
    public boolean performAsk(String _query) throws QueryExceptionHTTP;
    public boolean verifyConnection();
    public ReadyToVisualise ReadyToVisualise();
    public ArrayList<TripleStatement> GetStatements();
    public void UpdateModel() throws QueryExceptionHTTP;
    public void UpdateModel(String _newCenter, NodeType _type) throws QueryExceptionHTTP;
    public void setCenterVertex(String _newCenter, NodeType _type);
    public String getCenterVertex();
    public NodeType getNodeType(String _value);
    public TripleStatementWrapper InterestingSearch(
            String _bagOfWords, int _maxNodes, int _maxDist, 
            String _initNode, NodeType _initType, double _beta,
            String[] _filters, InterestingGraphThread _report,
            String _lang);
    public InterestingPath[] InterestingPaths(String[] _NSFilters, 
            String _source, String _target, 
            String _searchTerms, String _lang, 
            int _minPath, int _maxPath, int _maxNumOfPaths, InterestingPathsThread _report);
    
    public DisambiguityResults DisambiguitySearch(String _searchTerm, String _lang);
    
    public int getMaxVertices();
    public void setMaxVertices(int _max);
    public VisualiseWrapper CenterOn(String _center, NodeType _type);
    public VisualiseWrapper CenterOn();
    public ArrayList<InterestingPath> FetchPaths(String _query) throws Exception;
    public void setTimeout(int _timeout);
    public int getTimeout();
    public String getEndpointURL();
    public void setEndpointURL(String _newURI);
    
    // Vertex commands
    public ResourceResult UpdateVertexURI(String _vertex, 
            String _newURI);
    public ResourceResult VertexToBlankNode(String _vertex);
    public DeleteVertex DeleteVertex(String _node);
    public ResourceResult InsertVertex(String _value);
    public ArrayList<TripleStatement> CBD(String _vertex);
    
    // Blank Node commands
    public ResourceResult ResolveBlankNode(String _nodeID, 
            String _newURI);
    public DeleteVertex DeleteBlankNode(String _nodeID);
    public ResourceResult InsertBlankNode();
    
    // Literal commands
    public UpdateLiteralText UpdateLiteralText(String _literal, 
            String _newText);
    public DeleteLiteral DeleteLiteral(String _literal);
    public InsertLiteral InsertLiteral(String _text);
    public ArrayList<TripleStatement> SCBD(String _literal);
    public InterestingLiterals getLiterals(String _center, NodeType _centerType);
    
    // Edge commands
    public DeleteEdge DeleteEdge(String _model, 
            String _edgeURI, String _source, 
            NodeType _sourceType, String _target, 
            NodeType _targetType);
    public ResourceResult ChangeEdgeURI(String _edgeURI, 
            String _source, NodeType _sourceType, 
            String _target, NodeType _targetType, 
            String _newEdgeURI);
    public ResourceResult InsertEdge(String _edgeURI, 
            String _source, NodeType _sourceType, 
            String _target, NodeType _targetType);
}
