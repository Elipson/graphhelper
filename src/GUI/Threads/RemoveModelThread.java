/**
Copyright 2014 Dines Klewing Juul Madsen(thedrows@gmail.com), Heidi Olivia Munksgaard(heidi.olivi@gmail.com)

This file is part of GraphHelper.
GraphHelper is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
GraphHelper is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with GraphHelper. If not, see http://www.gnu.org/licenses/.
*/


package GUI.Threads;

import GUI.MainGUI;
import Graph.Enumerator.DeleteModel;
import javax.swing.SwingWorker;

/**
 *
 * @author Elipson
 */
public class RemoveModelThread extends SwingWorker<DeleteModel, Object> {

    private String model;
    
    public RemoveModelThread(String _model) {
        this.model = _model;
    }
    @Override
    protected DeleteModel doInBackground() throws Exception {
        
        DeleteModel result = MainGUI.myData.DeleteModel(model);
        
        return result;
    }
    
    @Override
    protected void done() {
        
        try {
            DeleteModel message = get();
            
            switch(message) {
                case Success:
                    MainGUI.UpdateLocalModels();
                    break;
                case ModelNotFound:
                    MainGUI.WriteToLog("Unable to finde " + this.model);
                    break;
                case Failure:
                    MainGUI.WriteToLog("An unknown error occurred while removing " + this.model);
                    break;
            }
        } catch (Exception e) {
            MainGUI.WriteToLog("An unknown error occurred while removing " + this.model);
        }
    }
}
