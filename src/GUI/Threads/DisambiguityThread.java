/**
Copyright 2014 Dines Klewing Juul Madsen(thedrows@gmail.com), Heidi Olivia Munksgaard(heidi.olivi@gmail.com)

This file is part of GraphHelper.
GraphHelper is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
GraphHelper is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with GraphHelper. If not, see http://www.gnu.org/licenses/.
*/

package GUI.Threads;

import GUI.MainGUI;
import Graph.InterestingPaths.DisambiguityResult;
import Graph.InterestingPaths.DisambiguityResults;
import GUI.Dialogs.InterestPathsDialog;
import Graph.Interfaces.DisambiguityDialog;
import javax.swing.SwingWorker;
import javax.swing.table.DefaultTableModel;

public class DisambiguityThread extends SwingWorker<DisambiguityResults, Void> {

    private final String myModel;
    private final DisambiguityDialog myDialog;

    public DisambiguityThread(String _model, DisambiguityDialog _dialog) {
        myModel = _model;
        myDialog = _dialog;
    }

    @Override
    protected DisambiguityResults doInBackground() throws Exception {
        DisambiguityResults result = MainGUI.myData.DisambiguitySearch(
                myModel, myDialog.getDisambiguitySearchTerm(), myDialog.getLanguage());
        return result;
    }

    @Override
    protected void done() {
        DisambiguityResults result;

        try {
            result = get();
            switch (result.actionReport) {
                case Success:
                    DefaultTableModel m = myDialog.getTableModel();

                    // Clearing table
                    m.setRowCount(0);

                    for (DisambiguityResult r : result.myResults) {
                        m.addRow(new Object[]{r.sbj, r.count});
                    }
                    myDialog.setLastSearch(result.myResults);
                    break;
                case Timeout:
                    myDialog.setConnError("Endpoint timed out during search");
                    break;
                default:
                    myDialog.setConnError("An unknown error occurred during search.");
                    break;
            }
        } catch (Exception e) {
            myDialog.setConnError("An unknown error occurred during search.");
        }
    }
}
