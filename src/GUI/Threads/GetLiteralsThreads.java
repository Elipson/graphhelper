/**
Copyright 2014 Dines Klewing Juul Madsen(thedrows@gmail.com), Heidi Olivia Munksgaard(heidi.olivi@gmail.com)

This file is part of GraphHelper.
GraphHelper is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
GraphHelper is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with GraphHelper. If not, see http://www.gnu.org/licenses/.
*/

package GUI.Threads;

import GUI.CustomControls.TabContainer.InterestingGraph;
import GUI.MainGUI;
import Graph.Enumerator.InterestingResult;
import Graph.Enumerator.NodeType;
import Graph.InterestingSearch.InterestingLiterals;
import java.util.concurrent.ExecutionException;
import javax.swing.SwingWorker;

public class GetLiteralsThreads extends SwingWorker<InterestingLiterals, Void>{

    private final String myModel;
    private final String myCenter;
    private final InterestingGraph myCaller;
    
    /**
     * Fetches literals from model
     * @param _model Model to search in
     * @param _center Center to search for
     * @param _caller Caller to return result to
     */
    public GetLiteralsThreads(String _model, String _center, InterestingGraph _caller) {
        myModel = _model;
        myCenter = _center;
        myCaller = _caller;
    }
    
    @Override
    protected InterestingLiterals doInBackground() throws Exception {
        InterestingLiterals result = new InterestingLiterals();
        result.actionReport = InterestingResult.UnknownError;
        
        result = MainGUI.myData.getLiterals(myModel, myCenter, NodeType.Vertex);
        return result;
    }
    
    @Override
    protected void done() {
        try {
            InterestingLiterals result = get();
            
            switch(result.actionReport) {
                case InitialNodeNotFound:
                    MainGUI.WriteToLog("Unable to find center vertex");
                    break;
                case ModelNotFound:
                    MainGUI.WriteToLog("Unable to find model");
                    break;
                case Success:
                    myCaller.InsertLiterals(result.myLiteral);
                    break;
                case Timeout:
                    MainGUI.WriteToLog("Connection timed out");
                    break;
                case UnknownError:
                    MainGUI.WriteToLog("Unknown error during literal fetch");
                    break;
                default:
                    MainGUI.WriteToLog("Unknown error during literal fetch");
                    break;
            }
        } catch (InterruptedException | ExecutionException e) {
            MainGUI.WriteToLog("Unknown error during literal fetch");
            MainGUI.WriteToLog(e.getMessage());
        }
    }
}
