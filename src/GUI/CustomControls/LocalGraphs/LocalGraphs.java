/**
Copyright 2014 Dines Klewing Juul Madsen(thedrows@gmail.com), Heidi Olivia Munksgaard(heidi.olivi@gmail.com)

This file is part of GraphHelper.
GraphHelper is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
GraphHelper is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with GraphHelper. If not, see http://www.gnu.org/licenses/.
*/

package GUI.CustomControls.LocalGraphs;

import GUI.Dialogs.EndpointSettingsDialog;
import GUI.Threads.RemoveModelThread;
import GUI.Threads.TestConnectionThread;
import GUI.Threads.VisualiseModelThread;
import GUI.Threads.InterestingPathsThread;
import GUI.Threads.ExportModelThread;
import GUI.MainGUI;
import Graph.Enumerator.FileFormat;
import Graph.Enumerator.NodeType;
import Graph.Implementations.GraphHelperUtil;
import GUI.Dialogs.InterestPathsDialog;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.File;
import java.io.IOException;
import javax.swing.DefaultListModel;
import javax.swing.JFileChooser;
import javax.swing.JList;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPopupMenu;
import javax.swing.SwingUtilities;

public class LocalGraphs extends JList implements MouseListener, ActionListener {

    public DefaultListModel<String> myModels = new DefaultListModel<>();
    private final JPopupMenu myLocalGraphMenu = new JPopupMenu();
    private final JPopupMenu myEndpointgraphMenu = new JPopupMenu();

    @SuppressWarnings("unchecked")
    public LocalGraphs() {
        // Connecting list of models and JList
        this.setModel(myModels);

        // Local graph menu
        JMenuItem menuItem = new JMenuItem("Rename Model");
        menuItem.addActionListener(this);
        myLocalGraphMenu.add(menuItem);

        menuItem = new JMenuItem("Export Model");
        menuItem.addActionListener(this);
        myLocalGraphMenu.add(menuItem);

        menuItem = new JMenuItem("Remove Model");
        menuItem.addActionListener(this);
        myLocalGraphMenu.add(menuItem);

        menuItem = new JMenuItem("Visualise");
        menuItem.addActionListener(this);
        myLocalGraphMenu.add(menuItem);

        menuItem = new JMenuItem("Set Focal Vertex");
        menuItem.addActionListener(this);
        myLocalGraphMenu.add(menuItem);

        menuItem = new JMenuItem("Set max nodes");
        menuItem.addActionListener(this);
        myLocalGraphMenu.add(menuItem);

        // Endpoint menu
        menuItem = new JMenuItem("Remove Endpoint");
        menuItem.addActionListener(this);
        myEndpointgraphMenu.add(menuItem);
        
        menuItem = new JMenuItem("Settings");
        menuItem.addActionListener(this);
        myEndpointgraphMenu.add(menuItem);
        
        menuItem = new JMenuItem("Test Connection");
        menuItem.addActionListener(this);
        myEndpointgraphMenu.add(menuItem);

        menuItem = new JMenuItem("Visualise");
        menuItem.addActionListener(this);
        myEndpointgraphMenu.add(menuItem);

        menuItem = new JMenuItem("Interesting Path Search");
        menuItem.addActionListener(this);
        myEndpointgraphMenu.add(menuItem);

        // Adding mouse listener to JMenu
        this.addMouseListener(this);
    }

    public void UpdateList() {
        this.myModels.clear();

        String[] models = MainGUI.myData.GetModelNames();

        if (models != null) {
            for (String model : models) {
                this.myModels.addElement(model);
            }
        }
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        if (SwingUtilities.isRightMouseButton(e) && myModels.isEmpty() == false) {
            this.setSelectedIndex(this.locationToIndex(e.getPoint()));

            boolean isLocal = MainGUI.myData.isLocalGraph((String) (String) this.getSelectedValue());
            if (isLocal == true) {
                myLocalGraphMenu.show(this, e.getX(), e.getY());
            } else {
                myEndpointgraphMenu.show(this, e.getX(), e.getY());
            }
        }
    }

    @Override
    public void mousePressed(MouseEvent e) {
    }

    @Override
    public void mouseReleased(MouseEvent e) {
    }

    @Override
    public void mouseEntered(MouseEvent e) {
    }

    @Override
    public void mouseExited(MouseEvent e) {
    }

    @Override
    public void actionPerformed(ActionEvent e) {

        JMenuItem source = (JMenuItem) (e.getSource());
        switch (source.getText()) {
            case "Rename Model":
                RenameModel(true);
                break;
            case "Export Model":
                ExportModel();
                break;
            case "Remove Model":
                RemoveModel();
                break;
            case "Visualise":
                VisualiseModel();
                break;
            case "Rename Endpoint":
                RenameModel(false);
                break;
            case "Remove Endpoint":
                RemoveModel();
                break;
            case "Test Connection":
                TestConnection();
                break;
            case "Set Focal Vertex":
                SetVertex();
                break;
            case "Set max nodes":
                setMaxNodes();
                break;
            case "Interesting Path Search":
                InterestingPathSearch();
                break;
            case "Settings":
                EndpointSettings();
                break;
        }
    }

    private void RenameModel(boolean isLocal) {

        String model = (String) this.getSelectedValue();
        String newName;

        if (isLocal == true) {
            newName = JOptionPane.showInputDialog(null, "Enter new name", "Rename Model", 1);
        } else {
            newName = JOptionPane.showInputDialog(null, "Enter new name", "Rename Endpoint", 1);
        }

        if (newName != null) {
            boolean result = MainGUI.myData.RenameModel(model, newName);

            if (result == true) {
                MainGUI.UpdateLocalModels();
            } else {
                MainGUI.WriteToLog("Unable to rename " + model);
            }
        }
    }

    private void ExportModel() {
        JFileChooser fc = new JFileChooser();

        String model = (String) this.getSelectedValue();
        int returnVal = fc.showSaveDialog(this);

        if (returnVal == JFileChooser.APPROVE_OPTION) {

            try {
                File file = fc.getSelectedFile();
                if (!file.exists()) {
                    file.createNewFile();
                }

                ExportModelThread newThread = new ExportModelThread(
                        model, file.getCanonicalPath(), FileFormat.RDF);

                newThread.execute();
            } catch (IOException e) {
            }
        }
    }

    private void RemoveModel() {

        String model = (String) this.getSelectedValue();
        Object[] yesno = {"Yes", "No"};

        //Custom button text
        JOptionPane confirmationPane = new JOptionPane();
        int selection = JOptionPane.showOptionDialog(this,
                "Remove " + model + "?",
                "Confirm Removal",
                JOptionPane.YES_NO_CANCEL_OPTION,
                JOptionPane.QUESTION_MESSAGE,
                null,
                yesno, yesno[1]);

        if (selection == 0) {
            RemoveModelThread myThread = new RemoveModelThread(model);
            myThread.execute();
        }
    }

    private void VisualiseModel() {
        String model = (String) this.getSelectedValue();

        VisualiseModelThread myThread = new VisualiseModelThread(model);
        myThread.execute();
    }

    private void TestConnection() {
        String model = (String) this.getSelectedValue();

        TestConnectionThread myThread = new TestConnectionThread(model);
        myThread.execute();
    }

    private void SetVertex() {
        String model = (String) this.getSelectedValue();
        String currentVertex = MainGUI.myData.getCenterVertex(model);

        String newVertex;
        if (currentVertex != null) {
            newVertex = JOptionPane.showInputDialog(null, "Enter new center vertex", currentVertex);
        } else {
            newVertex = JOptionPane.showInputDialog(null, "Enter new center vertex", "");
        }

        if (newVertex != null) {
            if (currentVertex.compareTo(newVertex) != 0 && GraphHelperUtil.isValidURI(newVertex)) {
                MainGUI.myData.setCentervertex(model, newVertex, NodeType.Vertex);
            } else {
                JOptionPane.showMessageDialog(
                        this,
                        "The entered focal point must be a valid URI",
                        "Invalid entry",
                        JOptionPane.ERROR_MESSAGE);
            }
        }
    }

    private void setMaxNodes() {
        String model = (String) getSelectedValue();

    }

    private void InterestingPathSearch() {
        String model = (String) this.getSelectedValue();
        InterestPathsDialog myDialog = new InterestPathsDialog(MainGUI.getFrame(), true, model);
        myDialog.setVisible(true);

        if (myDialog.isMyValidSearch()) {
            String[] NSFilters = myDialog.getNamespaceFilters();
            String source = myDialog.getMySourceNode();
            String target = myDialog.getMyTargetNode();
            String searchTerms = myDialog.getSearchTerm();
            String lang = myDialog.getMyLanguageRestriction();
            int minPath = myDialog.getMyMinPathLength();
            int maxPath = myDialog.getMyMaxPathLength();
            int maxNumOfPaths = myDialog.getMaxNumberOfPaths();

            InterestingPathsThread myThread = new InterestingPathsThread(
                    model,
                    NSFilters,
                    source,
                    target,
                    searchTerms,
                    lang,
                    minPath,
                    maxPath,
                    maxNumOfPaths
            );

            myThread.execute();
        }
    }

    private void EndpointSettings() {
        String model = (String) this.getSelectedValue();
        String vertex = MainGUI.myData.getCenterVertex(model);
        String endpointURL = MainGUI.myData.getEndpointURI(model);
        int maxNodes = MainGUI.myData.getMaxVertices(model);
        int timeout = MainGUI.myData.getTimeout(model);
        
        EndpointSettingsDialog dialog = new EndpointSettingsDialog(
                MainGUI.getFrame(), 
                true, 
                model, 
                endpointURL, 
                maxNodes, 
                timeout, 
                vertex);
        dialog.setVisible(true);
        
        if(dialog.isMyValidSearch()) {
            if(dialog.getMyName() == null ? model != null : !dialog.getMyName().equals(model)) {
                MainGUI.myData.RenameModel(model, dialog.getMyName());
            }
            if(dialog.getMyVertex() == null ? vertex != null : !dialog.getMyVertex().equals(vertex)) {
                MainGUI.myData.setCentervertex(model, dialog.getMyVertex(), NodeType.Vertex);
            }
            if(dialog.getMyEndpoint() == null ? endpointURL != null : !dialog.getMyEndpoint().equals(endpointURL)) {
                MainGUI.myData.setEndpointURI(model, dialog.getMyEndpoint());
            }
            if(dialog.getMyMaxNodes() != maxNodes) {
                MainGUI.myData.setMaxVertices(model, dialog.getMyMaxNodes());
            }
            if(dialog.getMyTimeout() != timeout) {
                MainGUI.myData.setTimeout(model, dialog.getMyTimeout());
            }
        }
    }
}
