/**
Copyright 2014 Dines Klewing Juul Madsen(thedrows@gmail.com), Heidi Olivia Munksgaard(heidi.olivi@gmail.com)

This file is part of GraphHelper.
GraphHelper is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
GraphHelper is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with GraphHelper. If not, see http://www.gnu.org/licenses/.
*/

package GUI.CustomControls.TabContainer;

import GUI.Threads.DescribeThread;
import GUI.Dialogs.InterestingGraphDialog;
import GUI.Threads.InterestingGraphThread;
import GUI.Threads.VisualiseModelThread;
import GUI.MainGUI;
import Graph.Enumerator.ChangeEdge;
import Graph.Enumerator.DeleteEdge;
import Graph.Enumerator.NodeType;
import Graph.Enumerator.DeleteVertex;
import Graph.Enumerator.UpdateVertexURI;
import Graph.Enumerator.VertexToBlankNode;
import Graph.Implementations.ResourceResult;
import Graph.Implementations.TripleStatement;
import Graph.Interfaces.GraphInterface;
import com.mxgraph.layout.mxFastOrganicLayout;
import com.mxgraph.model.mxCell;
import com.mxgraph.model.mxGraphModel;
import com.mxgraph.swing.mxGraphComponent;
import com.mxgraph.swing.util.mxMorphing;
import com.mxgraph.util.mxDomUtils;
import com.mxgraph.util.mxEvent;
import com.mxgraph.util.mxEventObject;
import com.mxgraph.view.mxGraph;
import com.mxgraph.view.mxStylesheet;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.SwingUtilities;
import javax.swing.event.PopupMenuEvent;
import javax.swing.event.PopupMenuListener;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class VisGraph extends mxGraph implements MouseListener, ActionListener, GraphInterface, PopupMenuListener {

    private mxGraphComponent myComponent;
    private mxFastOrganicLayout myLayout;
    private double myForceConstant = 50;
    private final JPopupMenu myNodeMenu = new JPopupMenu();
    private final JPopupMenu myBlankNodeMenu = new JPopupMenu();
    private final JPopupMenu myLiteralMenu = new JPopupMenu();
    private final JPopupMenu myEdgeMenu = new JPopupMenu();
    private final JPopupMenu myBackgroundMenu = new JPopupMenu();
    private final Document myDoc = mxDomUtils.createDocument();
    private final String myModelName;
    private final DistanceSlider mySlider = new DistanceSlider(30, 200, (int) myForceConstant);

    public VisGraph(ArrayList<TripleStatement> _stmts, String _modelName) {

        this.myModelName = _modelName;

        // <editor-fold defaultstate="collapsed" desc="Graph Settings">
        //this.myComponent = new mxGraphComponent(this);
        this.myComponent = new TrialmxGraphComponent(this);
        this.myComponent.setConnectable(false);
        this.setCellsEditable(false);
        this.setAllowDanglingEdges(false);
        this.setAllowLoops(false);
        this.setCellsDeletable(false);
        this.setCellsCloneable(false);
        this.setCellsDisconnectable(false);
        this.setDropEnabled(false);
        this.setSplitEnabled(false);
        this.setCellsBendable(false);
        this.setEdgeLabelsMovable(false);
        this.setVertexLabelsMovable(false);
        this.setCellsResizable(false);
        this.setAutoSizeCells(true);
        // </editor-fold>

        // <editor-fold defaultstate="collapsed" desc="Blank Node & Literal shapes">
        mxGraph graph = new mxGraph();
        mxStylesheet ss = this.getStylesheet();

        // Setting vertex style
        //ss.setDefaultVertexStyle(GraphStyles.getInterestingVertex());
        
        // Defining Literal node form
        ss.putCellStyle("Literal", GraphStyles.getLiteral());

        // Defining blank node form
        ss.putCellStyle("BlankNode", GraphStyles.getBlankNode());
        // </editor-fold>

        Object parent = this.getDefaultParent();
        this.getModel().beginUpdate();
        try {
            TripleStatement temp;
            Object v1, v2;
            for (int i = 0; i < _stmts.size(); i++) {
                temp = _stmts.get(i);

                // Handling Subject
                if (!this.ModelContainsCell(temp.mySubject)) {
                    if (temp.SubjectIsBlank == true) {
                        v1 = this.insertVertex(parent, temp.mySubject, "", 100, 100, 80, 30, "BlankNode");
                    } else {
                        v1 = this.insertVertex(parent, temp.mySubject, "", 100, 100, 80, 30);
                    }
                    this.myComponent.labelChanged(v1, temp.mySubjectShort, null);
                } else {
                    v1 = ((mxGraphModel) this.getModel()).getCell(temp.mySubject);
                }

                // Handling Object
                if (!this.ModelContainsCell(temp.myObject)) {
                    if (temp.ObjectIsBlank == true) {
                        v2 = this.insertVertex(parent, temp.myObject, "", 100, 100, 80, 30, "BlankNode");
                    } else if (temp.ObjectIsLiteral == true) {
                        v2 = this.insertVertex(parent, temp.myObject, "", 100, 100, 80, 30, "Literal");
                    } else {
                        v2 = this.insertVertex(parent, temp.myObject, "", 100, 100, 80, 30);
                    }

                    this.myComponent.labelChanged(v2, temp.myObjectShort, null);
                } else {
                    v2 = ((mxGraphModel) this.getModel()).getCell(temp.myObject);
                }

                
                Element edgeData = this.myDoc.createElement("edgeData");
                edgeData.setAttribute("fullURI", temp.myPredicate);
                edgeData.setAttribute("shortName", temp.myPredicateShort);
                this.insertEdge(parent, null, edgeData, v1, v2);
            }
        } finally {
            this.getModel().endUpdate();
        }

        // Constructing Vertex-right-click menu
        JMenuItem myItem = new JMenuItem("Copy Short Name");
        myItem.addActionListener(this);
        this.myNodeMenu.add(myItem);

        myItem = new JMenuItem("Copy URI");
        myItem.addActionListener(this);
        this.myNodeMenu.add(myItem);

        myItem = new JMenuItem("Center on");
        myItem.addActionListener(this);
        this.myNodeMenu.add(myItem);

        myItem = new JMenuItem("Change URI");
        myItem.addActionListener(this);
        this.myNodeMenu.add(myItem);

        myItem = new JMenuItem("Change to Blank Node");
        myItem.addActionListener(this);
        this.myNodeMenu.add(myItem);

        myItem = new JMenuItem("Remove Vertex");
        myItem.addActionListener(this);
        this.myNodeMenu.add(myItem);

        myItem = new JMenuItem("Describe");
        myItem.addActionListener(this);
        this.myNodeMenu.add(myItem);

        myItem = new JMenuItem("Search");
        myItem.addActionListener(this);
        this.myNodeMenu.add(myItem);

//-----------------------------------------------------------
        // Construct Blank Node right-click-menu
        myItem = new JMenuItem("Copy ID");
        myItem.addActionListener(this);
        this.myBlankNodeMenu.add(myItem);

        myItem = new JMenuItem("Center on");
        myItem.addActionListener(this);
        this.myBlankNodeMenu.add(myItem);

        myItem = new JMenuItem("Resolve");
        myItem.addActionListener(this);
        this.myBlankNodeMenu.add(myItem);

        myItem = new JMenuItem("Remove Blank Node");
        myItem.addActionListener(this);
        this.myBlankNodeMenu.add(myItem);

        myItem = new JMenuItem("Search");
        myItem.addActionListener(this);
        this.myBlankNodeMenu.add(myItem);

//-------------------------------------------------------
        // Construct Literal right-click-menu 
        myItem = new JMenuItem("Copy Text");
        myItem.addActionListener(this);
        this.myLiteralMenu.add(myItem);

        myItem = new JMenuItem("Center on");
        myItem.addActionListener(this);
        this.myLiteralMenu.add(myItem);

        myItem = new JMenuItem("Remove Literal");
        myItem.addActionListener(this);
        this.myLiteralMenu.add(myItem);

        myItem = new JMenuItem("Describe");
        myItem.addActionListener(this);
        this.myLiteralMenu.add(myItem);
//-------------------------------------------------------
        // Constructing Edge right-click menu
        myItem = new JMenuItem("Remove Edge");
        myItem.addActionListener(this);
        this.myEdgeMenu.add(myItem);

        myItem = new JMenuItem("Copy Name");
        myItem.addActionListener(this);
        this.myEdgeMenu.add(myItem);

        myItem = new JMenuItem("Copy ID");
        myItem.addActionListener(this);
        this.myEdgeMenu.add(myItem);

        myItem = new JMenuItem("Edit URI");
        myItem.addActionListener(this);
        this.myEdgeMenu.add(myItem);
//-----------------------------------------------------
        // Construct background right-click-menu
        myItem = new JMenuItem("Add Vertex");
        myItem.addActionListener(this);
        myBackgroundMenu.add(myItem);

        myItem = new JMenuItem("Add Blank Node");
        myItem.addActionListener(this);
        myBackgroundMenu.add(myItem);

        myItem = new JMenuItem("Add Literal");
        myItem.addActionListener(this);
        myBackgroundMenu.add(myItem);

        // Adding min. distance slider
        myBackgroundMenu.addSeparator();
        myBackgroundMenu.add(mySlider);

        myBackgroundMenu.addPopupMenuListener(this);
        this.myComponent.getGraphControl().addMouseListener(this);
        this.myComponent.setToolTips(true);
    }

    public void RearrangeModel() {

        this.myLayout = new mxFastOrganicLayout(this);
        this.myLayout.setForceConstant(myForceConstant);
        Object parent = this.getDefaultParent();

        this.getModel().beginUpdate();
        try {
            this.myLayout.execute(parent);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        } finally {
            mxMorphing morph = new mxMorphing(myComponent, 20, 1.2, 20);

            morph.addListener(mxEvent.DONE, new mxIEventListener() {

                @Override
                public void invoke(Object arg0, mxEventObject arg1) {
                    getModel().endUpdate();
                }
            });
            morph.startAnimation();
        }
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        if (SwingUtilities.isRightMouseButton(e)) {
            JScrollPane pane = ((JScrollPane) myComponent);
            int vertOffset = pane.getVerticalScrollBar().getValue();
            int horOffset = pane.getHorizontalScrollBar().getValue();

            Object[] myCells = this.getSelectionCells();
            if (myCells.length == 0) {
                this.myBackgroundMenu.show(
                        this.myComponent, e.getX() - horOffset, e.getY() - vertOffset);
            } else if (myCells.length == 1) {
                this.SingleSelectedNode(myCells[0], e);
            } else if (myCells.length > 1) {
                this.MultipleSelectedNodes(myCells, e);
            }
        }
    }

    /**
     * Removes a vertex or edge from the graph. If a vertex has edges connected,
     * these are deleted as well.
     *
     * @param _selection the cell to be removed
     */
    private void RemoveNode(mxCell _selection) {

        MainGUI.myData.DeleteVertex(this.myModelName, _selection.getId());
        this.setCellsDeletable(true);
        this.getModel().beginUpdate();
        try {
            Object[] items = {_selection};
            this.removeCells(items, true);
        } catch (Exception e) {
            System.out.println("Error!");
        } finally {
            this.getModel().endUpdate();
            this.refresh();
            this.setCellsDeletable(false);
        }
    }

    @Override
    public void mousePressed(MouseEvent e) {
    }

    @Override
    public void mouseReleased(MouseEvent e) {
    }

    @Override
    public void mouseEntered(MouseEvent e) {
        mxCell cell = (mxCell) this.myComponent.getCellAt(e.getX(), e.getY());
        if (cell == null) {

        }
    }

    @Override
    public void mouseExited(MouseEvent e) {

    }

    @Override
    public void actionPerformed(ActionEvent e) {
        JMenuItem item = (JMenuItem) e.getSource();
        switch (item.getText()) {
            case "Copy Short Name":     // Source: Vertex
                CopyNameToClipboard();
                break;
            case "Copy URI":            // Source: Vertex
                CopyIDToClipboard();
                break;
            case "Center on":           // Source: Node, BN, Literal
                CenterOn();
                break;
            case "Change URI":          // Source: Vertex
                ChangeURI();
                break;
            case "Change to Blank Node":// Source: Vertex
                ChangeToBlankNode();
                break;
            case "Remove Vertex":       // Source: Vertex
                RemoveNode();
                break;
            case "Copy ID":             // Source: Blank Node + Edge 
                CopyIDToClipboard();
                break;
            case "Resolve":             // Source: Blank Node
                ResolveBlankNode();
                break;
            case "Remove Blank Node":   // Source: Blank Node
                RemoveNode();
                break;
            case "Copy Text":           // Source: Literal
                CopyIDToClipboard();
                break;
            case "Remove Literal":      // Source: Literal
                RemoveNode();
                break;
            case "Remove Edge":         // Source: Edge
                RemoveEdge();
                break;
            case "Copy Name":           // Source: Edge
                CopyIDToClipboard();
                break;
            case "Add Vertex":          // Source: Background
                AddVertex();
                break;
            case "Add Blank Node":      // Source: Background
                AddBlankNode();
                break;
            case "Add Literal":         // Source: Background
                AddLiteral();
                break;
            case "Edit URI":            // Source: Edge
                EditEdgeURI();
                break;
            case "Describe":
                Describe();
                break;
            case "Search":
                InterestingSearch();
                break;
        }
    }

    /**
     *
     * @param cell to return tool tip for
     * @return Returns long form URI/Blank Node ID/Literal
     */
    @Override
    public String getToolTipForCell(Object cell) {
        if (cell instanceof mxCell) {
            mxCell c = (mxCell) cell;

            if (c.isVertex()) {
                return c.getId();
            }
            if (c.isEdge()) {
                try {
                    Object value = c.getValue();
                    Element elt = (Element) value;
                    return elt.getAttribute("fullURI");
                } catch (Exception e) {
                    return "Cell Element not found";
                }
            }
        } else {
            return super.getToolTipForCell(cell);
        }
        return null;
    }

    private boolean ModelContainsCell(String _URI) {
        Object myCell = ((mxGraphModel) this.getModel()).getCell(_URI);

        if (myCell == null) {
            return false;
        } else {
            return true;
        }
    }

    @Override
    public mxGraphComponent GetGraphComponent() {
        return this.myComponent;
    }

    @Override
    public String convertValueToString(Object cell) {
        if (cell instanceof mxCell) {
            Object value = ((mxCell) cell).getValue();

            if (value instanceof Element) {
                Element elt = (Element) value;

                if (elt.getTagName().equalsIgnoreCase("edgeData")) {
                    return elt.getAttribute("shortName");
                }
            }
        }
        return super.convertValueToString(cell);
    }

    private void CopyIDToClipboard() {
        mxCell cell = (mxCell) this.getSelectionCell();

        if (cell.isVertex()) {
            StringSelection sel
                    = new StringSelection((String) cell.getId());
            Clipboard cb = Toolkit.getDefaultToolkit().getSystemClipboard();
            cb.setContents(sel, sel);
        }
        if (cell.isEdge()) {
            Object value = cell.getValue();
            Element elt = (Element) value;
            String URI = elt.getAttribute("fullURI");

            StringSelection sel
                    = new StringSelection(URI);
            Clipboard cb = Toolkit.getDefaultToolkit().getSystemClipboard();
            cb.setContents(sel, sel);
        }
    }

    private void CopyNameToClipboard() {
        mxCell cell = (mxCell) this.getSelectionCell();

        if (cell.isVertex()) {
            try {
                StringSelection sel = new StringSelection((String) cell.getValue());
                Clipboard cb = Toolkit.getDefaultToolkit().getSystemClipboard();
                cb.setContents(sel, sel);
            } catch (Exception e) {
            }
        } else if (cell.isEdge()) {
            try {
                Element elt = (Element) cell.getValue();

                StringSelection sel = new StringSelection(elt.getAttribute("shortName"));
                Clipboard cb = Toolkit.getDefaultToolkit().getSystemClipboard();
                cb.setContents(sel, sel);
            } catch (Exception e) {
            }
        }
    }

    /**
     * Clears all vertices and edges from the graph
     */
    public void Clear() {

        this.setCellsDeletable(true);
        this.getModel().beginUpdate();
        try {
            ((mxGraphModel) this.getModel()).clear();
        } catch (Exception e) {
            System.out.println("Error!");
        } finally {
            this.getModel().endUpdate();
            this.refresh();
            this.setCellsDeletable(false);
        }
    }

    /**
     * Inserts the provided triples into the graph
     *
     * @param _stmts triples to be inserted
     */
    public void UpdateGraph(ArrayList<TripleStatement> _stmts) {
        Object parent = this.getDefaultParent();
        this.getModel().beginUpdate();
        try {
            TripleStatement temp;
            Object v1, v2;
            for (int i = 0; i < _stmts.size(); i++) {
                temp = _stmts.get(i);

                // Handling Subject
                if (!this.ModelContainsCell(temp.mySubject)) {
                    if (temp.SubjectIsBlank == true) {
                        v1 = this.insertVertex(parent, temp.mySubject, "", 100, 100, 80, 30, "BlankNode");
                    } else {
                        v1 = this.insertVertex(parent, temp.mySubject, "", 100, 100, 80, 30);
                    }
                    this.myComponent.labelChanged(v1, temp.mySubjectShort, null);
                } else {
                    v1 = ((mxGraphModel) this.getModel()).getCell(temp.mySubject);
                }

                // Handling Object
                if (!this.ModelContainsCell(temp.myObject)) {
                    if (temp.ObjectIsBlank == true) {
                        v2 = this.insertVertex(parent, temp.myObject, "", 100, 100, 80, 30, "BlankNode");
                    } else if (temp.ObjectIsLiteral == true) {
                        v2 = this.insertVertex(parent, temp.myObject, "", 100, 100, 80, 30, "Literal");
                    } else {
                        v2 = this.insertVertex(parent, temp.myObject, "", 100, 100, 80, 30);
                    }

                    this.myComponent.labelChanged(v2, temp.myObjectShort, null);
                } else {
                    v2 = ((mxGraphModel) this.getModel()).getCell(temp.myObject);
                }

                Element edgeData = this.myDoc.createElement("edgeData");
                edgeData.setAttribute("fullURI", temp.myPredicate);
                edgeData.setAttribute("shortName", temp.myPredicateShort);
                this.insertEdge(parent, null, edgeData, v1, v2);
            }
        } finally {
            this.getModel().endUpdate();
        }
    }

    /**
     * Centers on the model's current center, showing it's neighbours
     */
    private void CenterOn() {
        mxCell cell = (mxCell) this.getSelectionCell();

        if (cell.isVertex()) {
            VisualiseModelThread myThread = new VisualiseModelThread(myModelName);
            myThread.execute();
        }
    }

    /**
     * Centers on the selected node, showing it's neighbours
     *
     * @param _cell Node to be centered on
     */
    public void CenterOn(mxCell _cell) {

        if (_cell.isVertex()) {
            VisualiseModelThread myThread = new VisualiseModelThread(myModelName, _cell.getId(), getNodeType(_cell));
            myThread.execute();
        }
    }

    // @todo implement and change to dual + multiple
    private void MultipleSelectedNodes(Object[] _cells, MouseEvent _e) {

        mxCell source = (mxCell) _cells[0];
        mxCell target = (mxCell) _cells[1];

        if (source.isVertex() && target.isVertex()) {

        }
    }

    private void SingleSelectedNode(Object _cell, MouseEvent _e) {
        String nodeStyle = ((mxCell) _cell).getStyle();
        JScrollPane pane = ((JScrollPane) myComponent);
        int vertOffset = pane.getVerticalScrollBar().getValue();
        int horOffset = pane.getHorizontalScrollBar().getValue();

        if (nodeStyle == null) {
            myNodeMenu.show(
                    myComponent, _e.getX() - horOffset, _e.getY() - vertOffset);
        } else if (nodeStyle.compareTo("Literal") == 0) {
            myLiteralMenu.show(
                    myComponent, _e.getX() - horOffset, _e.getY() - vertOffset);
        } else if (nodeStyle.compareTo("BlankNode") == 0) {
            myBlankNodeMenu.show(
                    myComponent, _e.getX() - horOffset, _e.getY() - vertOffset);
        } else if (((mxCell) _cell).isEdge()) {
            myEdgeMenu.show(
                    myComponent, _e.getX() - horOffset, _e.getY() - vertOffset);
        }
    }

    /**
     * Source: Vertex Changes URI for one specific vertex
     */
    private void ChangeURI() {
        mxCell currentVertex = (mxCell) getSelectionCell();
        String newURI = JOptionPane.showInputDialog("Enter new URI", currentVertex.getId());
        if (newURI != null) {
            ResourceResult result = MainGUI.myData.UpdateVertexURI(myModelName, currentVertex.getId(), newURI);

            switch ((UpdateVertexURI) result.actionReport) {
                case Success:
                    MainGUI.WriteToLog("URI updated");
                    currentVertex.setId(result.nodeValue);
                    myComponent.labelChanged(currentVertex, result.nodeValueShort, null);
                    break;
                case ChangeDenied:
                    MainGUI.WriteToLog("Unable to change URI. Access denied");
                    break;
                case DuplicateURLNotAllowed:
                    MainGUI.WriteToLog("Duplicates not allowed. Model already contains URI");
                    break;
                case InvalidURI:
                    MainGUI.WriteToLog("Entered value was not a valid URI");
                    break;

            }
        }
    }

    private void ChangeToBlankNode() {
        mxCell currentVertex = (mxCell) getSelectionCell();
        ResourceResult result = MainGUI.myData.VertexToBlankNode(myModelName, currentVertex.getId());
        switch ((VertexToBlankNode) result.actionReport) {
            case Success:
                MainGUI.WriteToLog("Vertex changed to blank node");
                currentVertex.setId(result.nodeValue);
                myComponent.labelChanged(currentVertex, "BN", null);
                currentVertex.setStyle("BlankNode");
                refresh();
                break;
            case ChangeDenied:
                MainGUI.WriteToLog("Unable to change URI. Access denied");
                break;
        }
    }

    private void RemoveNode() {
        mxCell currentVertex = (mxCell) getSelectionCell();
        DeleteVertex result = MainGUI.myData.DeleteVertex(myModelName, currentVertex.getId());
        switch (result) {
            case Success:
                MainGUI.WriteToLog("Vertex deleted");
                Object[] items = {currentVertex};
                this.removeCells(items, true);
                break;
            case ChangeDenied:
                MainGUI.WriteToLog("Unable to change URI. Access denied");
                break;
        }
    }

    private void ResolveBlankNode() {
        mxCell currentVertex = (mxCell) getSelectionCell();
        String newURI = JOptionPane.showInputDialog("Enter URI", "");
        if (newURI != null) {
            ResourceResult result = MainGUI.myData.ResolveBlankNode(
                    myModelName,
                    currentVertex.getId(),
                    newURI);

            switch ((UpdateVertexURI) result.actionReport) {
                case Success:
                    MainGUI.WriteToLog("Blank Node resolved");
                    currentVertex.setId(result.nodeValue);
                    myComponent.labelChanged(currentVertex, result.nodeValueShort, null);
                    currentVertex.setStyle(null);
                    refresh();
                    break;
                case DuplicateURLNotAllowed:
                    MainGUI.WriteToLog(newURI + " already exists within model");
                    break;
                case InvalidURI:
                    MainGUI.WriteToLog("Text entered is not a valid URI");
                    break;
                case ChangeDenied:
                    MainGUI.WriteToLog("Unable to change URI. Access denied");
                    break;
            }
        }
    }

    private void RemoveEdge() {
        mxCell currentEdge = (mxCell) getSelectionCell();

        // Payload for method
        String source, target, edgeURI;
        NodeType sourceType, targetType;

        // Getting edge URI
        Object value = currentEdge.getValue();
        Element elt = (Element) value;
        edgeURI = elt.getAttribute("fullURI");

        // Getting source info
        source = currentEdge.getSource().getId();
        sourceType = getNodeType((mxCell) currentEdge.getSource());

        // Getting target info
        target = currentEdge.getTarget().getId();
        targetType = getNodeType((mxCell) currentEdge.getTarget());

        // Execute change
        DeleteEdge result = MainGUI.myData.DeleteEdge(myModelName, edgeURI, source, sourceType, target, targetType);

        // Report changes
        switch (result) {
            case Success:
                MainGUI.WriteToLog("Edge deleted");
                getModel().beginUpdate();
                try {
                    getModel().remove(currentEdge);
                } catch (Exception e) {

                } finally {
                    getModel().endUpdate();
                    refresh();
                }
                break;
            case ChangeDenied:
                MainGUI.WriteToLog("Unable to change URI. Access denied");
                break;
        }
    }

    private void AddVertex() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    private void AddBlankNode() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    private void AddLiteral() {

    }

    private void EditEdgeURI() {
        mxCell currentEdge = (mxCell) getSelectionCell();

        // Payload for method
        String source, target, edgeURI;
        NodeType sourceType, targetType;

        // Getting edge URI
        Object value = currentEdge.getValue();
        Element elt = (Element) value;
        edgeURI = elt.getAttribute("fullURI");

        // Getting source info
        source = currentEdge.getSource().getId();
        sourceType = getNodeType((mxCell) currentEdge.getSource());

        // Getting target info
        target = currentEdge.getTarget().getId();
        targetType = getNodeType((mxCell) currentEdge.getTarget());

        String newURI = JOptionPane.showInputDialog(myComponent, "Enter new URI", edgeURI);
        if (newURI != null) {
            ResourceResult result = MainGUI.myData.ChangeEdgeURI(myModelName, edgeURI, source, sourceType, target, targetType, newURI);

            switch ((ChangeEdge) result.actionReport) {
                case Success:
                    MainGUI.WriteToLog("Edge URI changed");
                    myComponent.labelChanged(currentEdge, result.nodeValueShort, null);
                    elt.setAttribute("fullURI", result.nodeValue);
                    refresh();
                    break;
                case ChangeDenied:
                    MainGUI.WriteToLog("Unable to change URI. Access denied");
                    break;
                case InvalidURI:
                    MainGUI.WriteToLog("URI entered is not a valid URI");
                    break;
            }
        }
    }

    private NodeType getNodeType(mxCell _cell) {
        String nodeStyle = _cell.getStyle();
        if (nodeStyle == null) {
            return NodeType.Vertex;
        } else if (nodeStyle.compareTo("Literal") == 0) {
            return NodeType.Literal;
        } else if (nodeStyle.compareTo("BlankNode") == 0) {
            return NodeType.BlankNode;
        } else {
            return NodeType.Edge;
        }
    }

    private void Describe() {
        mxCell myCell = (mxCell) getSelectionCell();
        NodeType a = getNodeType(myCell);
        if (a == NodeType.Literal) {
            DescribeThread thread = new DescribeThread(myCell.getId(), myModelName, NodeType.Literal);
            thread.execute();
        } else if (a == NodeType.Vertex) {
            DescribeThread thread = new DescribeThread(myCell.getId(), myModelName, NodeType.Vertex);
            thread.execute();
        }
    }

    private void InterestingSearch() {
        mxCell myCell = (mxCell) getSelectionCell();

        InterestingGraphDialog dialog = new InterestingGraphDialog(
                MainGUI.getFrame(),
                true,
                myModelName,
                myCell.getId()
        );
        dialog.setVisible(true);

        // user has entered valid data and pressed search?
        if (dialog.isPerformSearch() == true) {
            MainGUI.WriteToLog("Performing search. This may take a while..");

            InterestingGraphThread myThread = new InterestingGraphThread(
                    this.myModelName,
                    dialog.getSearchTerms(),
                    dialog.getMyMaxNodes(),
                    dialog.getMyDistance(),
                    myCell.getId(),
                    getNodeType(myCell),
                    dialog.getBetaValue(),
                    dialog.getNamespaces(),
                    dialog.getMyLanguage()
            );

            myThread.execute();
        }
    }

    @Override
    public void popupMenuWillBecomeVisible(PopupMenuEvent e) {

    }

    @Override
    public void popupMenuWillBecomeInvisible(PopupMenuEvent e) {
        if (mySlider.valueHasChanged()) {
            int newValue = mySlider.Update();

            // Updating force
            myForceConstant = (double) newValue;

            // Refreshing visualisation
            RearrangeModel();
        }
    }

    @Override
    public void popupMenuCanceled(PopupMenuEvent e) {

    }
}
