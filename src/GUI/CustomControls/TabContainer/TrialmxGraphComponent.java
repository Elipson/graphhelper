/**
Copyright 2014 Dines Klewing Juul Madsen(thedrows@gmail.com), Heidi Olivia Munksgaard(heidi.olivi@gmail.com)

This file is part of GraphHelper.
GraphHelper is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
GraphHelper is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with GraphHelper. If not, see http://www.gnu.org/licenses/.
*/

package GUI.CustomControls.TabContainer;

import com.mxgraph.model.mxCell;
import com.mxgraph.swing.mxGraphComponent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class TrialmxGraphComponent extends mxGraphComponent {

    VisGraph myGraph;

    public TrialmxGraphComponent(VisGraph _graph) {
        super(_graph);
        myGraph = _graph;
        installDoubleClickHandler();

        graphControl.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseReleased(MouseEvent e) {
                if (!e.isConsumed() && isEditEvent(e)) {
                    Object cell = getCellAt(e.getX(), e.getY(), false);
                    if (cell != null && ((mxCell) cell).isVertex()) {
                        System.out.println("cell=" + graph.getLabel(cell));
                        myGraph.CenterOn((mxCell)cell);
                    }
                    e.consume();
                }
            }
        });
    }
}
