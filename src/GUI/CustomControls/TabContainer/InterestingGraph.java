/**
Copyright 2014 Dines Klewing Juul Madsen(thedrows@gmail.com), Heidi Olivia Munksgaard(heidi.olivi@gmail.com)

This file is part of GraphHelper.
GraphHelper is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
GraphHelper is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with GraphHelper. If not, see http://www.gnu.org/licenses/.
*/

package GUI.CustomControls.TabContainer;

import GUI.MainGUI;
import GUI.Threads.GetLiteralsThreads;
import Graph.Enumerator.InterestingResult;
import Graph.Enumerator.NodeType;
import Graph.InterestingSearch.InterestingLiterals;
import Graph.InterestingSearch.InterestingTriple;
import Graph.InterestingSearch.LiteralTriple;
import Graph.Interfaces.GraphInterface;
import com.mxgraph.layout.mxFastOrganicLayout;
import com.mxgraph.model.mxCell;
import com.mxgraph.model.mxGraphModel;
import com.mxgraph.swing.mxGraphComponent;
import com.mxgraph.swing.util.mxMorphing;
import com.mxgraph.util.mxConstants;
import com.mxgraph.util.mxDomUtils;
import com.mxgraph.util.mxEvent;
import com.mxgraph.util.mxEventObject;
import com.mxgraph.util.mxUtils;
import com.mxgraph.view.mxGraph;
import com.mxgraph.view.mxStylesheet;
import java.awt.Color;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Map;
import java.util.Set;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.SwingUtilities;
import javax.swing.event.PopupMenuEvent;
import javax.swing.event.PopupMenuListener;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class InterestingGraph extends mxGraph implements PopupMenuListener, MouseListener, ActionListener, GraphInterface {

    private mxGraphComponent myComponent = new mxGraphComponent(this);
    private double myForceConstant = 50;
    private Document myDoc = mxDomUtils.createDocument();
    private mxFastOrganicLayout myLayout;
    private String myModel;
    private final JPopupMenu myMenu = new JPopupMenu();
    private final JPopupMenu myBackground = new JPopupMenu();

    private HashSet<String> hasBeenExplored = new HashSet<>();
    private final DistanceSlider mySlider = new DistanceSlider(30, 200, (int) myForceConstant);
    private ArrayList<InterestingTriple> myData;

    public InterestingGraph(String _model, ArrayList<InterestingTriple> _triples) {
        myModel = _model;
        myData = _triples;

        // <editor-fold defaultstate="collapsed" desc="Graph Settings">
        this.myComponent = new mxGraphComponent(this);
        this.myComponent.setConnectable(false);
        this.setCellsEditable(false);
        this.setAllowDanglingEdges(false);
        this.setAllowLoops(false);
        this.setCellsDeletable(false);
        this.setCellsCloneable(false);
        this.setCellsDisconnectable(false);
        this.setDropEnabled(false);
        this.setSplitEnabled(false);
        this.setCellsBendable(false);
        this.setEdgeLabelsMovable(false);
        this.setVertexLabelsMovable(false);
        this.setCellsResizable(false);
        this.setAutoSizeCells(true);
        this.myComponent.setToolTips(true);
        // </editor-fold>

        Hashtable<String, Object> style = new Hashtable<>();
        mxStylesheet stylesheet = this.getStylesheet();

        stylesheet.putCellStyle("withMatch", GraphStyles.getVertexWithScore());
        stylesheet.putCellStyle("withoutMatch", GraphStyles.getVertexWithoutScore());

        // Defining Literal node form
        style = new Hashtable<>();
        style.put(mxConstants.STYLE_SHAPE, mxConstants.SHAPE_RECTANGLE);
        style.put(mxConstants.STYLE_FILLCOLOR, mxUtils.getHexColorString(Color.yellow));
        style.put(mxConstants.STYLE_PERIMETER, mxConstants.PERIMETER_RECTANGLE);
        style.put(mxConstants.STYLE_STROKECOLOR, mxUtils.getHexColorString(Color.black));
        style.put(mxConstants.STYLE_SPACING_LEFT, 5);
        style.put(mxConstants.STYLE_SPACING_RIGHT, 2);
        style.put(mxConstants.STYLE_SPACING_TOP, 3);
        style.put(mxConstants.STYLE_SPACING_BOTTOM, 1);
        stylesheet.putCellStyle("Literal", style);

        Object parent = this.getDefaultParent();
        this.getModel().beginUpdate();
        try {
            Object v1, v2;
            for (InterestingTriple it : _triples) {

                // Handling first element
                if (!this.ModelContainsCell(it.nodeOne)) {
                    InterestingValue iv = new InterestingValue();
                    iv.URI = it.nodeOne;
                    iv.shortURI = it.nodeOneShort;
                    iv.score = String.format("%.2f", it.nodeValueOne);// (Double) it.nodeValueOne).toString();
                    if (it.nodeValueOne > 0) {

                        v1 = this.insertVertex(parent, it.nodeOne, iv, 100, 100, 80, 30, "withMatch");
                    } else {
                        v1 = this.insertVertex(parent, it.nodeOne, iv, 100, 100, 80, 30, "withoutMatch");
                    }
                    updateCellSize(v1);
                } else {
                    v1 = ((mxGraphModel) this.getModel()).getCell(it.nodeOne);
                }

                // Handling second element
                if (!this.ModelContainsCell(it.nodeTwo)) {
                    InterestingValue iv = new InterestingValue();
                    iv.URI = it.nodeTwo;
                    iv.shortURI = it.nodeTwoShort;
                    iv.score = String.format("%.2f", it.nodeValueTwo);//(Double)it.nodeValueTwo).toString();
                    if (it.nodeValueTwo > 0) {
                        v2 = this.insertVertex(parent, it.nodeTwo, iv, 100, 100, 80, 30, "withMatch");
                    } else {
                        v2 = this.insertVertex(parent, it.nodeTwo, iv, 100, 100, 80, 30, "withoutMatch");
                    }
                    updateCellSize(v2);
                } else {
                    v2 = ((mxGraphModel) this.getModel()).getCell(it.nodeTwo);
                }

                // Handling predicate
                String predShort = it.predicateShorts[0];
                String predLong = it.predicateValues[0];

                Element edgeData = myDoc.createElement("edgeData");

                for (int i = 1; i < it.predicateValues.length; i++) {
                    if (i < 3) {
                        predShort += "\n" + it.predicateShorts[i];
                    }
                    predLong += "<br>" + it.predicateValues[i];
                }
                edgeData.setAttribute("longForm", "<html>" + predLong + "</html>");
                edgeData.setAttribute("shortForm", predShort);

                switch (it.predDir) {
                    case source2Target:
                        insertEdge(parent, null, edgeData, v1, v2);
                        break;
                    case target2Source:
                        insertEdge(parent, null, edgeData, v2, v1);
                        break;
                    case uniDirection:
                        insertEdge(parent, null, edgeData, v1, v2, "startArrow=none;endArrow=none;");
                        break;
                }
            }

        } finally {
            this.getModel().endUpdate();
        }

        RearrangeModel();

        // Constructing menus
        JMenuItem myItem = new JMenuItem("Copy Value");
        myItem.addActionListener(this);
        this.myMenu.add(myItem);

        myItem = new JMenuItem("Get Literals");
        myItem.addActionListener(this);
        this.myMenu.add(myItem);

        myItem = new JMenuItem("Export to XML");
        myItem.addActionListener(this);
        this.myBackground.add(myItem);

        // Adding min. distance slider
        myBackground.addSeparator();
        myBackground.add(mySlider);

        myBackground.addPopupMenuListener(this);
        this.myComponent.getGraphControl().addMouseListener(this);
    }

    public void RearrangeModel() {
        this.myLayout = new mxFastOrganicLayout(this);
        this.myLayout.setForceConstant(myForceConstant);
        Object parent = this.getDefaultParent();

        this.getModel().beginUpdate();
        try {
            this.myLayout.execute(parent);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        } finally {
            mxMorphing morph = new mxMorphing(myComponent, 20, 1.2, 20);

            morph.addListener(mxEvent.DONE, new mxIEventListener() {

                @Override
                public void invoke(Object arg0, mxEventObject arg1) {
                    getModel().endUpdate();
                }
            });
            morph.startAnimation();
        }
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        if (SwingUtilities.isRightMouseButton(e)) {
            JScrollPane pane = ((JScrollPane) myComponent);
            int vertOffset = pane.getVerticalScrollBar().getValue();
            int horOffset = pane.getHorizontalScrollBar().getValue();

            Object[] myCells = this.getSelectionCells();
            if (myCells.length == 0) {
                this.myBackground.show(
                        this.myComponent, e.getX() - horOffset, e.getY() - vertOffset);
            } else if (myCells.length == 1) {
                this.SingleSelectedNode(myCells[0], e);
            } else if (myCells.length > 1) {
                this.MultipleSelectedNodes(myCells, e);
            }
        }
    }

    @Override
    public void mousePressed(MouseEvent e) {

    }

    @Override
    public void mouseReleased(MouseEvent e) {

    }

    @Override
    public void mouseEntered(MouseEvent e) {
    }

    @Override
    public void mouseExited(MouseEvent e) {
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        JMenuItem item = (JMenuItem) e.getSource();
        switch (item.getText()) {
            case "Get Literals":
                GetLiterals();
                break;
            case "Copy Value":
                CopyValue();
                break;
            case "Export to XML":
                Export2XML();
                break;
        }
    }

    @Override
    public String convertValueToString(Object cell) {

        if (cell instanceof mxCell) {
            mxCell myCell = (mxCell) cell;
            if (myCell.isVertex()) {
                InterestingValue iv = (InterestingValue) myCell.getValue();
                return iv.shortURI;
            } else {
                Element value = (Element) myCell.getValue();
                if (value instanceof Element) {
                    Element elt = (Element) value;

                    if (elt.getTagName().equalsIgnoreCase("edgeData")) {
                        return elt.getAttribute("shortForm");
                    }
                }
            }

        }
        return super.convertValueToString(cell);
    }

    private boolean ModelContainsCell(String _URI) {
        Object myCell = ((mxGraphModel) this.getModel()).getCell(_URI);

        if (myCell == null) {
            return false;
        } else {
            return true;
        }
    }

    /**
     *
     * @param cell to return tool tip for
     * @return Returns long form URI/Blank Node ID/Literal
     */
    @Override
    public String getToolTipForCell(Object cell) {
        if (cell instanceof mxCell) {
            mxCell c = (mxCell) cell;

            if (c.isVertex()) {
                InterestingValue iv = (InterestingValue) c.getValue();
                String out = "<HTML>";
                out += "URI: " + iv.URI;
                out += "<br>Score: " + iv.score;
                out += "</HTML>";
                return out;
            }
            if (c.isEdge()) {
                try {
                    Object value = c.getValue();
                    Element elt = (Element) value;
                    return elt.getAttribute("longForm");
                } catch (Exception e) {
                    return "Cell Element not found";
                }
            }
        } else {
            return super.getToolTipForCell(cell);
        }
        return null;
    }

    private void SingleSelectedNode(Object object, MouseEvent e) {
        JScrollPane pane = ((JScrollPane) myComponent);
        int vertOffset = pane.getVerticalScrollBar().getValue();
        int horOffset = pane.getHorizontalScrollBar().getValue();
        myMenu.show(this.myComponent, e.getX() - horOffset, e.getY() - vertOffset);
    }

    private void MultipleSelectedNodes(Object[] myCells, MouseEvent e) {
        //myMenu.show(myComponent, e.getX(), e.getY());
    }

    private void GetLiterals() {
        Object[] myCells = getSelectionCells();

        for (Object o : myCells) {
            mxCell c = (mxCell) o;
            if (c.isVertex()) {
                InterestingLiterals result;
                InterestingValue iv = (InterestingValue)c.getValue();
                
                GetLiteralsThreads thread = new GetLiteralsThreads(myModel, iv.URI, this);
                thread.execute();
                
//                result = MainGUI.myData.getLiterals(myModel, iv.URI, NodeType.Vertex);
//
//                if (result.actionReport == InterestingResult.Success) {
//                    InsertLiterals(result.myLiteral);
//                }
            }
        }
    }

    private void CopyValue() {
        Object[] myCells = this.getSelectionCells();
        String result = "";

        if (myCells.length == 1) {
            if (((mxCell) myCells[0]).isVertex()) {
                result += ((mxCell) myCells[0]).getId();
            }
        } else if (myCells.length > 1) {
            for (Object c : myCells) {
                if (((mxCell) c).isVertex()) {
                    result += ((mxCell) c).getId() + "\n";
                }
            }
        }
        try {
            StringSelection sel = new StringSelection(result);
            Clipboard cb = Toolkit.getDefaultToolkit().getSystemClipboard();
            cb.setContents(sel, sel);
        } catch (Exception e) {
        }
    }

    @Override
    public mxGraphComponent GetGraphComponent() {
        return this.myComponent;
    }

    /**
     * Inserts an array of literal triples
     *
     * @param _literals
     */
    public void InsertLiterals(ArrayList<LiteralTriple> _literals) {
        if (!_literals.isEmpty()) {
            // Has the node already been expanded?
            if (!hasBeenExplored.contains(_literals.get(0).sbj)) {
                Object parent = this.getDefaultParent();
                for (LiteralTriple tl : _literals) {
                    Object sbj, obj;
                    if (ModelContainsCell(tl.sbj)) {
                        // Getting subject
                        sbj = ((mxGraphModel) this.getModel()).getCell(tl.sbj);

                        // Creating or getting literal
                        if (ModelContainsCell(tl.lit)) {
                            obj = ((mxGraphModel) this.getModel()).getCell(tl.lit);
                        } else {
                            InterestingValue iv = new InterestingValue();
                            iv.URI = tl.lit;
                            iv.shortURI = tl.litShort;
                            iv.score = "N/A";
                            obj = insertVertex(parent, tl.lit, iv, 100, 100, 80, 30, "Literal");
                        }
                        
                        updateCellSize(obj);

                        // Creating edge
                        Element edgeData = this.myDoc.createElement("edgeData");
                        edgeData.setAttribute("longForm", tl.pred);
                        edgeData.setAttribute("shortForm", tl.predShort);
                        insertEdge(parent, null, edgeData, sbj, obj);
                    }
                }
            }
            RearrangeModel();
        }
    }

    @Override
    public void popupMenuWillBecomeVisible(PopupMenuEvent e) {
    }

    @Override
    public void popupMenuWillBecomeInvisible(PopupMenuEvent e) {
        if (mySlider.valueHasChanged()) {
            int newValue = mySlider.Update();

            // Updating force
            myForceConstant = (double) newValue;

            // Refreshing visualisation
            RearrangeModel();
        }
    }

    @Override
    public void popupMenuCanceled(PopupMenuEvent e) {
    }

    private void Export2XML() {
        Set<String> distinctURIs = new HashSet<>();
        Map<String, Double> distinctScores = new HashMap<>();

        for (InterestingTriple it : myData) {
            distinctURIs.add(it.nodeOne);
            distinctScores.put(it.nodeOne, it.nodeValueOne);
            distinctURIs.add(it.nodeTwo);
            distinctScores.put(it.nodeTwo, it.nodeValueTwo);
        }

        try {
            DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = docFactory.newDocumentBuilder();

            Document doc = docBuilder.newDocument();
            Element rootElement = doc.createElement("Graph");
            doc.appendChild(rootElement);

            for (String s : distinctURIs) {
                Element node = doc.createElement("node");
                rootElement.appendChild(node);

                Element URI = doc.createElement("URI");
                URI.appendChild(doc.createTextNode(s));

                Element score = doc.createElement("Score");
                score.appendChild(doc.createTextNode(((Double) distinctScores.get(s)).toString()));
                node.appendChild(score);

                node.appendChild(URI);
            }

            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            transformer.setOutputProperty(OutputKeys.METHOD, "xml");
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");
            transformer.setOutputProperty(OutputKeys.VERSION, "1.0");
            transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
            DOMSource source = new DOMSource(doc);
            File output = new File("InterestingGrapgDump.xml");
            StreamResult result = new StreamResult(output);

            transformer.transform(source, result);
            MainGUI.WriteToLog("Paths exported to: " + output.getAbsolutePath());
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
}
