EXPAND_MODEL_OUTWARDS(URIs, filters) ::=
<<
SELECT ?s ?p ?o
WHERE {
	VALUES ?s {<URIs: {uri| \<<uri>\>}> }
	?s ?p ?o .
	FILTER(!isLiteral(?o)) .
	FILTER(!isBlank(?o)) .
	<if(filters)>
	<filters: {f|
	FILTER(!strStarts(STR(?o), "<f>")) .
	FILTER(!strStarts(STR(?p), "<f>")) .}>
	<endif>
}
>>

EXPAND_MODEL_INWARDS(URIs, filters) ::=
<<
SELECT ?s ?p ?o
WHERE {
	VALUES ?o {<URIs: {uri| \<<uri>\>}> }
	?s ?p ?o .
	FILTER(!isLiteral(?s)) .
	FILTER(!isBlank(?s)) .
	<if(filters)>
	<filters: {f|
	FILTER(!strStarts(STR(?s), "<f>")) .
	FILTER(!strStarts(STR(?p), "<f>")) .}>
	<endif>
}
>>

OUTBOUND(URI, filters, offset) ::=
<<
SELECT (\<<URI>\> AS ?known) ?p ?o WHERE
{
	\<<URI>\> ?p ?o .
	FILTER(!isBlank(?o)) .
	FILTER(!isLiteral(?o)) .
	<if(filters)>
	<filters: {f|
	FILTER(!strStarts(STR(?o), "<f>")) .
	FILTER(!strStarts(STR(?p), "<f>")) .}>
	<endif>
}
<if(offset)>OFFSET <offset><endif>
LIMIT 10000
>>

INBOUND(URI, filters, offset) ::=
<<
SELECT ?s ?p (\<<URI>\> AS ?known) WHERE
{
	?s ?p \<<URI>\> .
	FILTER(!isBlank(?s)) .
	FILTER(!isLiteral(?s)) .
	<if(filters)>
	<filters: {f|
FILTER(!strStarts(STR(?s), "<f>")) .
FILTER(!strStarts(STR(?p), "<f>")) .}>
	<endif>
}
<if(offset)>OFFSET <offset><endif>
LIMIT 10000
>>

INANDOUT(URI, filters) ::=
<<
SELECT (\<<URI>\> AS ?input) ?p1 ?p2 ?o ?s WHERE 
{
	{
		\<<URI>\> ?p1 ?o
		FILTER(!isBlank(?o)) .
		FILTER(!isLiteral(?o)) .
		
		<if(filters)>
		<filters: {f|
		FILTER(!strStarts(STR(?o), "<f>")) .
		FILTER(!strStarts(STR(?p1), "<f>")) .}>
		<endif>
	}
	UNION
	{
		?s ?p2 \<<URI>\> .
		FILTER(!isBlank(?s)) .
		FILTER(!isLiteral(?s)) .
		
		<if(filters)>
		<filters: {f|
		FILTER(!strStarts(STR(?s), "<f>")) .
		FILTER(!strStarts(STR(?p2), "<f>")) .}>
		<endif>
	}
}
>>

INANDOUT_MULTIPLE(URI, filters) ::=
<<
SELECT (\<<URI>\> AS ?input) ?p1 ?p2 ?o ?s WHERE 
{
	VALUES ?known {}
	{
		\<<URI>\> ?p1 ?o
		FILTER(!isBlank(?o)) .
		FILTER(!isLiteral(?o)) .
		
		<if(filters)>
		<filters: {f|
		FILTER(!strStarts(STR(?o), "<f>")) .
		FILTER(!strStarts(STR(?p1), "<f>")) .}>
		<endif>
	}
	UNION
	{
		?s ?p2 \<<URI>\> .
		FILTER(!isBlank(?s)) .
		FILTER(!isLiteral(?s)) .
		
		<if(filters)>
		<filters: {f|
		FILTER(!strStarts(STR(?s), "<f>")) .
		FILTER(!strStarts(STR(?p2), "<f>")) .}>
		<endif>
	}
}
>>

BULKFETCH(URIs, filters, offset) ::=
<<
SELECT ?known ?p1 ?p2 ?o ?s WHERE 
{
VALUES ?known {<URIs: {uri|\<<uri>\>}>}
{
?known ?p1 ?o
FILTER(!isBlank(?o)) .
FILTER(!isLiteral(?o)) .
<if(filters)>
<filters: {f|
FILTER(!strStarts(STR(?o), "<f>")) .
FILTER(!strStarts(STR(?p1), "<f>")) .}>
<endif>
}
UNION
{
?s ?p2 ?known .
FILTER(!isBlank(?s)) .
FILTER(!isLiteral(?s)) .
<if(filters)>
<filters: {f|
FILTER(!strStarts(STR(?s), "<f>")) .
FILTER(!strStarts(STR(?p2), "<f>")) .}>
<endif>
}
}
ORDER BY ?known
<if(offset)>OFFSET <offset><endif>
LIMIT 10000
>>

COUNT_TRIPLES(URIs, filters) ::=
<<
SELECT ?known (COUNT(?known) AS ?count) WHERE 
{
	VALUES ?known {<URIs: {uri|\<<uri>\>}>} .
	{
		?known ?p ?o .
		FILTER(!isLiteral(?o)) .
		FILTER(!isBlank(?o)) .
		<if(filters)>
		<filters: {f|
		FILTER(!strStarts(STR(?o), "<f>")) .
		FILTER(!strStarts(STR(?p), "<f>")) .}>
		<endif>
	}
	UNION
	{
		?s ?p ?known .
		FILTER(!isLiteral(?s)) .
		FILTER(!isBlank(?s)) .
		<if(filters)>
		<filters: {f|
		FILTER(!strStarts(STR(?s), "<f>")) .
		FILTER(!strStarts(STR(?p), "<f>")) .}>
		<endif>
	}
} GROUP BY ?known
>>